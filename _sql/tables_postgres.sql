create table "act_evt_log" (
  "log_nr_" int8 not null,
  "type_" varchar(64),
  "proc_def_id_" varchar(64),
  "proc_inst_id_" varchar(64),
  "execution_id_" varchar(64),
  "task_id_" varchar(64),
  "time_stamp_" timestamp not null,
  "user_id_" varchar(255),
  "data_" bytea,
  "lock_owner_" varchar(255),
  "lock_time_" timestamp,
  "is_processed_" int2,
  primary key ("log_nr_")
);

create table "act_ge_bytearray" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "name_" varchar(255),
  "deployment_id_" varchar(64),
  "bytes_" bytea,
  "generated_" int2,
  constraint "_copy_125" primary key ("id_")
);
create index "act_fk_bytearr_depl" on "act_ge_bytearray" using btree (
  "deployment_id_" asc
);

create table "act_ge_property" (
  "name_" varchar(64) not null,
  "value_" varchar(300),
  "rev_" int4,
  constraint "_copy_124" primary key ("name_")
);

create table "act_hi_actinst" (
  "id_" varchar(64) not null,
  "proc_def_id_" varchar(64) not null,
  "proc_inst_id_" varchar(64) not null,
  "execution_id_" varchar(64) not null,
  "act_id_" varchar(255) not null,
  "task_id_" varchar(64),
  "call_proc_inst_id_" varchar(64),
  "act_name_" varchar(255),
  "act_type_" varchar(255) not null,
  "assignee_" varchar(255),
  "start_time_" timestamp not null,
  "end_time_" timestamp,
  "duration_" int8,
  "delete_reason_" varchar(4000),
  "tenant_id_" varchar(255),
  constraint "_copy_123" primary key ("id_")
);
create index "act_idx_hi_act_inst_start" on "act_hi_actinst" using btree (
  "start_time_" asc
);
create index "act_idx_hi_act_inst_end" on "act_hi_actinst" using btree (
  "end_time_" asc
);
create index "act_idx_hi_act_inst_procinst" on "act_hi_actinst" using btree (
  "proc_inst_id_" asc,
  "act_id_" asc
);
create index "act_idx_hi_act_inst_exec" on "act_hi_actinst" using btree (
  "execution_id_" asc,
  "act_id_" asc
);

create table "act_hi_attachment" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "user_id_" varchar(255),
  "name_" varchar(255),
  "description_" varchar(4000),
  "type_" varchar(255),
  "task_id_" varchar(64),
  "proc_inst_id_" varchar(64),
  "url_" varchar(4000),
  "content_id_" varchar(64),
  "time_" timestamp,
  constraint "_copy_122" primary key ("id_")
);

create table "act_hi_comment" (
  "id_" varchar(64) not null,
  "type_" varchar(255),
  "time_" timestamp not null,
  "user_id_" varchar(255),
  "task_id_" varchar(64),
  "proc_inst_id_" varchar(64),
  "action_" varchar(255),
  "message_" varchar(4000),
  "full_msg_" bytea,
  constraint "_copy_121" primary key ("id_")
);

create table "act_hi_detail" (
  "id_" varchar(64) not null,
  "type_" varchar(255) not null,
  "proc_inst_id_" varchar(64),
  "execution_id_" varchar(64),
  "task_id_" varchar(64),
  "act_inst_id_" varchar(64),
  "name_" varchar(255) not null,
  "var_type_" varchar(255),
  "rev_" int4,
  "time_" timestamp not null,
  "bytearray_id_" varchar(64),
  "double_" float8,
  "long_" int8,
  "text_" varchar(4000),
  "text2_" varchar(4000),
  constraint "_copy_120" primary key ("id_")
);
create index "act_idx_hi_detail_proc_inst" on "act_hi_detail" using btree (
  "proc_inst_id_" asc
);
create index "act_idx_hi_detail_act_inst" on "act_hi_detail" using btree (
  "act_inst_id_" asc
);
create index "act_idx_hi_detail_time" on "act_hi_detail" using btree (
  "time_" asc
);
create index "act_idx_hi_detail_name" on "act_hi_detail" using btree (
  "name_" asc
);
create index "act_idx_hi_detail_task_id" on "act_hi_detail" using btree (
  "task_id_" asc
);

create table "act_hi_identitylink" (
  "id_" varchar(64) not null,
  "group_id_" varchar(255),
  "type_" varchar(255),
  "user_id_" varchar(255),
  "task_id_" varchar(64),
  "proc_inst_id_" varchar(64),
  constraint "_copy_119" primary key ("id_")
);
create index "act_idx_hi_ident_lnk_user" on "act_hi_identitylink" using btree (
  "user_id_" asc
);
create index "act_idx_hi_ident_lnk_task" on "act_hi_identitylink" using btree (
  "task_id_" asc
);
create index "act_idx_hi_ident_lnk_procinst" on "act_hi_identitylink" using btree (
  "proc_inst_id_" asc
);

create table "act_hi_procinst" (
  "id_" varchar(64) not null,
  "proc_inst_id_" varchar(64) not null,
  "business_key_" varchar(255),
  "proc_def_id_" varchar(64) not null,
  "start_time_" timestamp not null,
  "end_time_" timestamp,
  "duration_" int8,
  "start_user_id_" varchar(255),
  "start_act_id_" varchar(255),
  "end_act_id_" varchar(255),
  "super_process_instance_id_" varchar(64),
  "delete_reason_" varchar(4000),
  "tenant_id_" varchar(255),
  "name_" varchar(255),
  constraint "_copy_118" primary key ("id_")
);
create unique index "proc_inst_id_" on "act_hi_procinst" using btree (
  "proc_inst_id_" asc
);
create index "act_idx_hi_pro_inst_end" on "act_hi_procinst" using btree (
  "end_time_" asc
);
create index "act_idx_hi_pro_i_buskey" on "act_hi_procinst" using btree (
  "business_key_" asc
);

create table "act_hi_taskinst" (
  "id_" varchar(64) not null,
  "proc_def_id_" varchar(64),
  "task_def_key_" varchar(255),
  "proc_inst_id_" varchar(64),
  "execution_id_" varchar(64),
  "name_" varchar(255),
  "parent_task_id_" varchar(64),
  "description_" varchar(4000),
  "owner_" varchar(255),
  "assignee_" varchar(255),
  "start_time_" timestamp not null,
  "claim_time_" timestamp,
  "end_time_" timestamp,
  "duration_" int8,
  "delete_reason_" varchar(4000),
  "priority_" int4,
  "due_date_" timestamp,
  "form_key_" varchar(255),
  "category_" varchar(255),
  "tenant_id_" varchar(255),
  constraint "_copy_117" primary key ("id_")
);
create index "act_idx_hi_task_inst_procinst" on "act_hi_taskinst" using btree (
  "proc_inst_id_" asc
);

create table "act_hi_varinst" (
  "id_" varchar(64) not null,
  "proc_inst_id_" varchar(64),
  "execution_id_" varchar(64),
  "task_id_" varchar(64),
  "name_" varchar(255) not null,
  "var_type_" varchar(100),
  "rev_" int4,
  "bytearray_id_" varchar(64),
  "double_" float8,
  "long_" int8,
  "text_" varchar(4000),
  "text2_" varchar(4000),
  "create_time_" timestamp,
  "last_updated_time_" timestamp,
  constraint "_copy_116" primary key ("id_")
);
create index "act_idx_hi_procvar_proc_inst" on "act_hi_varinst" using btree (
  "proc_inst_id_" asc
);
create index "act_idx_hi_procvar_name_type" on "act_hi_varinst" using btree (
  "name_" asc,
  "var_type_" asc
);
create index "act_idx_hi_procvar_task_id" on "act_hi_varinst" using btree (
  "task_id_" asc
);

create table "act_id_group" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "name_" varchar(255),
  "type_" varchar(255),
  constraint "_copy_115" primary key ("id_")
);

create table "act_id_info" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "user_id_" varchar(64),
  "type_" varchar(64),
  "key_" varchar(255),
  "value_" varchar(255),
  "password_" bytea,
  "parent_id_" varchar(255),
  constraint "_copy_114" primary key ("id_")
);

create table "act_id_membership" (
  "user_id_" varchar(64) not null,
  "group_id_" varchar(64) not null,
  constraint "_copy_113" primary key ("user_id_", "group_id_")
);
create index "act_fk_memb_group" on "act_id_membership" using btree (
  "group_id_" asc
);

create table "act_id_user" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "first_" varchar(255),
  "last_" varchar(255),
  "email_" varchar(255),
  "pwd_" varchar(255),
  "picture_id_" varchar(64),
  constraint "_copy_112" primary key ("id_")
);

create table "act_procdef_info" (
  "id_" varchar(64) not null,
  "proc_def_id_" varchar(64) not null,
  "rev_" int4,
  "info_json_id_" varchar(64),
  constraint "_copy_111" primary key ("id_")
);
create unique index "act_uniq_info_procdef" on "act_procdef_info" using btree (
  "proc_def_id_" asc
);
create index "act_idx_info_procdef" on "act_procdef_info" using btree (
  "proc_def_id_" asc
);
create index "act_fk_info_json_ba" on "act_procdef_info" using btree (
  "info_json_id_" asc
);

create table "act_re_deployment" (
  "id_" varchar(64) not null,
  "name_" varchar(255),
  "category_" varchar(255),
  "key_" varchar(255),
  "tenant_id_" varchar(255),
  "deploy_time_" timestamp,
  "engine_version_" varchar(255),
  constraint "_copy_110" primary key ("id_")
);

create table "act_re_model" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "name_" varchar(255),
  "key_" varchar(255),
  "category_" varchar(255),
  "create_time_" timestamp,
  "last_update_time_" timestamp,
  "version_" int4,
  "meta_info_" varchar(4000),
  "deployment_id_" varchar(64),
  "editor_source_value_id_" varchar(64),
  "editor_source_extra_value_id_" varchar(64),
  "tenant_id_" varchar(255),
  constraint "_copy_109" primary key ("id_")
);
create index "act_fk_model_source" on "act_re_model" using btree (
  "editor_source_value_id_" asc
);
create index "act_fk_model_source_extra" on "act_re_model" using btree (
  "editor_source_extra_value_id_" asc
);
create index "act_fk_model_deployment" on "act_re_model" using btree (
  "deployment_id_" asc
);

create table "act_re_procdef" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "category_" varchar(255),
  "name_" varchar(255),
  "key_" varchar(255) not null,
  "version_" int4 not null,
  "deployment_id_" varchar(64),
  "resource_name_" varchar(4000),
  "dgrm_resource_name_" varchar(4000),
  "description_" varchar(4000),
  "has_start_form_key_" int2,
  "has_graphical_notation_" int2,
  "suspension_state_" int4,
  "tenant_id_" varchar(255),
  "engine_version_" varchar(255),
  constraint "_copy_108" primary key ("id_")
);
create unique index "act_uniq_procdef" on "act_re_procdef" using btree (
  "key_" asc,
  "version_" asc,
  "tenant_id_" asc
);

create table "act_ru_deadletter_job" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "type_" varchar(255) not null,
  "exclusive_" int2,
  "execution_id_" varchar(64),
  "process_instance_id_" varchar(64),
  "proc_def_id_" varchar(64),
  "exception_stack_id_" varchar(64),
  "exception_msg_" varchar(4000),
  "duedate_" timestamp,
  "repeat_" varchar(255),
  "handler_type_" varchar(255),
  "handler_cfg_" varchar(4000),
  "tenant_id_" varchar(255),
  constraint "_copy_107" primary key ("id_")
);
create index "act_fk_deadletter_job_execution" on "act_ru_deadletter_job" using btree (
  "execution_id_" asc
);
create index "act_fk_deadletter_job_process_instance" on "act_ru_deadletter_job" using btree (
  "process_instance_id_" asc
);
create index "act_fk_deadletter_job_proc_def" on "act_ru_deadletter_job" using btree (
  "proc_def_id_" asc
);
create index "act_fk_deadletter_job_exception" on "act_ru_deadletter_job" using btree (
  "exception_stack_id_" asc
);

create table "act_ru_event_subscr" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "event_type_" varchar(255) not null,
  "event_name_" varchar(255),
  "execution_id_" varchar(64),
  "proc_inst_id_" varchar(64),
  "activity_id_" varchar(64),
  "configuration_" varchar(255),
  "created_" timestamp not null,
  "proc_def_id_" varchar(64),
  "tenant_id_" varchar(255),
  constraint "_copy_106" primary key ("id_")
);
create index "act_idx_event_subscr_config_" on "act_ru_event_subscr" using btree (
  "configuration_" asc
);
create index "act_fk_event_exec" on "act_ru_event_subscr" using btree (
  "execution_id_" asc
);

create table "act_ru_execution" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "proc_inst_id_" varchar(64),
  "business_key_" varchar(255),
  "parent_id_" varchar(64),
  "proc_def_id_" varchar(64),
  "super_exec_" varchar(64),
  "root_proc_inst_id_" varchar(64),
  "act_id_" varchar(255),
  "is_active_" int2,
  "is_concurrent_" int2,
  "is_scope_" int2,
  "is_event_scope_" int2,
  "is_mi_root_" int2,
  "suspension_state_" int4,
  "cached_ent_state_" int4,
  "tenant_id_" varchar(255),
  "name_" varchar(255),
  "start_time_" timestamp,
  "start_user_id_" varchar(255),
  "lock_time_" timestamp,
  "is_count_enabled_" int2,
  "evt_subscr_count_" int4,
  "task_count_" int4,
  "job_count_" int4,
  "timer_job_count_" int4,
  "susp_job_count_" int4,
  "deadletter_job_count_" int4,
  "var_count_" int4,
  "id_link_count_" int4,
  constraint "_copy_105" primary key ("id_")
);
create index "act_idx_exec_buskey" on "act_ru_execution" using btree (
  "business_key_" asc
);
create index "act_idc_exec_root" on "act_ru_execution" using btree (
  "root_proc_inst_id_" asc
);
create index "act_fk_exe_procinst" on "act_ru_execution" using btree (
  "proc_inst_id_" asc
);
create index "act_fk_exe_parent" on "act_ru_execution" using btree (
  "parent_id_" asc
);
create index "act_fk_exe_super" on "act_ru_execution" using btree (
  "super_exec_" asc
);
create index "act_fk_exe_procdef" on "act_ru_execution" using btree (
  "proc_def_id_" asc
);

create table "act_ru_identitylink" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "group_id_" varchar(255),
  "type_" varchar(255),
  "user_id_" varchar(255),
  "task_id_" varchar(64),
  "proc_inst_id_" varchar(64),
  "proc_def_id_" varchar(64),
  constraint "_copy_104" primary key ("id_")
);
create index "act_idx_ident_lnk_user" on "act_ru_identitylink" using btree (
  "user_id_" asc
);
create index "act_idx_ident_lnk_group" on "act_ru_identitylink" using btree (
  "group_id_" asc
);
create index "act_idx_athrz_procedef" on "act_ru_identitylink" using btree (
  "proc_def_id_" asc
);
create index "act_fk_tskass_task" on "act_ru_identitylink" using btree (
  "task_id_" asc
);
create index "act_fk_idl_procinst" on "act_ru_identitylink" using btree (
  "proc_inst_id_" asc
);

create table "act_ru_job" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "type_" varchar(255) not null,
  "lock_exp_time_" timestamp,
  "lock_owner_" varchar(255),
  "exclusive_" int2,
  "execution_id_" varchar(64),
  "process_instance_id_" varchar(64),
  "proc_def_id_" varchar(64),
  "retries_" int4,
  "exception_stack_id_" varchar(64),
  "exception_msg_" varchar(4000),
  "duedate_" timestamp,
  "repeat_" varchar(255),
  "handler_type_" varchar(255),
  "handler_cfg_" varchar(4000),
  "tenant_id_" varchar(255),
  constraint "_copy_103" primary key ("id_")
);
create index "act_fk_job_execution" on "act_ru_job" using btree (
  "execution_id_" asc
);
create index "act_fk_job_process_instance" on "act_ru_job" using btree (
  "process_instance_id_" asc
);
create index "act_fk_job_proc_def" on "act_ru_job" using btree (
  "proc_def_id_" asc
);
create index "act_fk_job_exception" on "act_ru_job" using btree (
  "exception_stack_id_" asc
);

create table "act_ru_suspended_job" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "type_" varchar(255) not null,
  "exclusive_" int2,
  "execution_id_" varchar(64),
  "process_instance_id_" varchar(64),
  "proc_def_id_" varchar(64),
  "retries_" int4,
  "exception_stack_id_" varchar(64),
  "exception_msg_" varchar(4000),
  "duedate_" timestamp,
  "repeat_" varchar(255),
  "handler_type_" varchar(255),
  "handler_cfg_" varchar(4000),
  "tenant_id_" varchar(255),
  constraint "_copy_102" primary key ("id_")
);
create index "act_fk_suspended_job_execution" on "act_ru_suspended_job" using btree (
  "execution_id_" asc
);
create index "act_fk_suspended_job_process_instance" on "act_ru_suspended_job" using btree (
  "process_instance_id_" asc
);
create index "act_fk_suspended_job_proc_def" on "act_ru_suspended_job" using btree (
  "proc_def_id_" asc
);
create index "act_fk_suspended_job_exception" on "act_ru_suspended_job" using btree (
  "exception_stack_id_" asc
);

create table "act_ru_task" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "execution_id_" varchar(64),
  "proc_inst_id_" varchar(64),
  "proc_def_id_" varchar(64),
  "name_" varchar(255),
  "parent_task_id_" varchar(64),
  "description_" varchar(4000),
  "task_def_key_" varchar(255),
  "owner_" varchar(255),
  "assignee_" varchar(255),
  "delegation_" varchar(64),
  "priority_" int4,
  "create_time_" timestamp,
  "due_date_" timestamp,
  "category_" varchar(255),
  "suspension_state_" int4,
  "tenant_id_" varchar(255),
  "form_key_" varchar(255),
  "claim_time_" timestamp,
  constraint "_copy_101" primary key ("id_")
);
create index "act_idx_task_create" on "act_ru_task" using btree (
  "create_time_" asc
);
create index "act_fk_task_exe" on "act_ru_task" using btree (
  "execution_id_" asc
);
create index "act_fk_task_procinst" on "act_ru_task" using btree (
  "proc_inst_id_" asc
);
create index "act_fk_task_procdef" on "act_ru_task" using btree (
  "proc_def_id_" asc
);

create table "act_ru_timer_job" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "type_" varchar(255) not null,
  "lock_exp_time_" timestamp,
  "lock_owner_" varchar(255),
  "exclusive_" int2,
  "execution_id_" varchar(64),
  "process_instance_id_" varchar(64),
  "proc_def_id_" varchar(64),
  "retries_" int4,
  "exception_stack_id_" varchar(64),
  "exception_msg_" varchar(4000),
  "duedate_" timestamp,
  "repeat_" varchar(255),
  "handler_type_" varchar(255),
  "handler_cfg_" varchar(4000),
  "tenant_id_" varchar(255),
  constraint "_copy_100" primary key ("id_")
);
create index "act_fk_timer_job_execution" on "act_ru_timer_job" using btree (
  "execution_id_" asc
);
create index "act_fk_timer_job_process_instance" on "act_ru_timer_job" using btree (
  "process_instance_id_" asc
);
create index "act_fk_timer_job_proc_def" on "act_ru_timer_job" using btree (
  "proc_def_id_" asc
);
create index "act_fk_timer_job_exception" on "act_ru_timer_job" using btree (
  "exception_stack_id_" asc
);

create table "act_ru_variable" (
  "id_" varchar(64) not null,
  "rev_" int4,
  "type_" varchar(255) not null,
  "name_" varchar(255) not null,
  "execution_id_" varchar(64),
  "proc_inst_id_" varchar(64),
  "task_id_" varchar(64),
  "bytearray_id_" varchar(64),
  "double_" float8,
  "long_" int8,
  "text_" varchar(4000),
  "text2_" varchar(4000),
  constraint "_copy_99" primary key ("id_")
);
create index "act_idx_variable_task_id" on "act_ru_variable" using btree (
  "task_id_" asc
);
create index "act_fk_var_exe" on "act_ru_variable" using btree (
  "execution_id_" asc
);
create index "act_fk_var_procinst" on "act_ru_variable" using btree (
  "proc_inst_id_" asc
);
create index "act_fk_var_bytearray" on "act_ru_variable" using btree (
  "bytearray_id_" asc
);

create table "logistics_info" (
  "id" int4 not null,
  "shipper_code" varchar(64),
  "logistic_code" varchar(128),
  "success" int2,
  "reason" varchar(256),
  "state" int2,
  "push_id" int4,
  "state_ex" int2,
  "location" varchar(64),
  constraint "_copy_98" primary key ("id")
);
comment on column "logistics_info"."id" is '主键';
comment on column "logistics_info"."shipper_code" is '快递公司编码';
comment on column "logistics_info"."logistic_code" is '快递单号';
comment on column "logistics_info"."success" is '成功与否 0：失败 1：成功';
comment on column "logistics_info"."reason" is '失败原因';
comment on column "logistics_info"."state" is '普通物流状态';
comment on column "logistics_info"."push_id" is '推送id';
comment on column "logistics_info"."state_ex" is '物流状态';
comment on column "logistics_info"."location" is '当前城市';
comment on table "logistics_info" is '物流详情';

create table "logistics_placing_order" (
  "id" int4 not null,
  "order_id" varchar(64),
  "update_time" timestamp,
  "printtemplate" text,
  constraint "_copy_97" primary key ("id")
);
comment on column "logistics_placing_order"."id" is '主键';
comment on column "logistics_placing_order"."order_id" is '订单id';
comment on column "logistics_placing_order"."update_time" is '更新时间';
comment on column "logistics_placing_order"."printtemplate" is '模板';
comment on table "logistics_placing_order" is '下单模版';

create table "logistics_push" (
  "id" int4 not null,
  "count" varchar(64),
  "push_time" timestamp,
  constraint "_copy_96" primary key ("id")
);
comment on column "logistics_push"."id" is '主键';
comment on column "logistics_push"."count" is '推送统计';
comment on column "logistics_push"."push_time" is '推送时间';
comment on table "logistics_push" is '推送记录';

create table "logistics_sender" (
  "sender_id" int4 not null,
  "name" varchar(64),
  "mobile" varchar(64),
  "post_code" varchar(64),
  "province_name" varchar(64),
  "city_name" varchar(64),
  "exp_area_name" varchar(64),
  "address" varchar(512),
  "is_default" int2,
  constraint "_copy_95" primary key ("sender_id")
);
comment on column "logistics_sender"."sender_id" is '主键';
comment on column "logistics_sender"."name" is '发件人';
comment on column "logistics_sender"."mobile" is '电话';
comment on column "logistics_sender"."post_code" is '发件地邮编';
comment on column "logistics_sender"."province_name" is '发件省';
comment on column "logistics_sender"."city_name" is '发件市';
comment on column "logistics_sender"."exp_area_name" is '发件区/县';
comment on column "logistics_sender"."address" is '发件人详细地址';
comment on column "logistics_sender"."is_default" is '是否默认';
comment on table "logistics_sender" is '发件人';

create table "logistics_traces" (
  "traces_id" int4 not null,
  "accept_time" timestamp,
  "accept_station" varchar(512),
  "logistics_info_id" int4,
  "remark" varchar(512),
  "location" varchar(64),
  "action" varchar(64),
  constraint "_copy_94" primary key ("traces_id")
);
comment on column "logistics_traces"."traces_id" is '主键';
comment on column "logistics_traces"."accept_time" is '轨迹发生时间';
comment on column "logistics_traces"."accept_station" is '轨迹描述';
comment on column "logistics_traces"."logistics_info_id" is '物流详情id';
comment on column "logistics_traces"."remark" is '备注';
comment on column "logistics_traces"."location" is '历史节点所在城市';
comment on column "logistics_traces"."action" is '物流状态编码';
comment on table "logistics_traces" is '物流轨迹';

create table "mall_account_log" (
  "id" varchar(32) not null,
  "user_id" varchar(32) not null,
  "type" int2 not null,
  "price" numeric(10,2) not null,
  "log_desc" text not null,
  "add_time" timestamp not null,
  "order_type" int2 not null,
  "order_sn" varchar(32),
  "from_type" int2,
  constraint "_copy_93" primary key ("id")
);
comment on column "mall_account_log"."id" is '主键';
comment on column "mall_account_log"."type" is '类型：1=收入，2=支出';
comment on column "mall_account_log"."price" is '变动金额';
comment on column "mall_account_log"."log_desc" is '变动说明';
comment on column "mall_account_log"."order_type" is '订单类型 0--充值 1--商城订单 2--秒杀订单 3--拼团订单 4--商城订单退款 5--秒杀订单退款 6--拼团订单退款';
comment on column "mall_account_log"."order_sn" is '订单编号';
comment on column "mall_account_log"."from_type" is '用户下单来源类型 1:微信小程序 2:微信公众号 3:app 4:h5 5:支付宝小程序';
comment on table "mall_account_log" is '用户账户余额变动记录';

create table "mall_address" (
  "id" varchar(32) not null,
  "user_id" varchar(32) not null,
  "user_name" varchar(64),
  "mobile" varchar(32),
  "postal_code" varchar(64),
  "national_code" varchar(64),
  "province_name" varchar(64),
  "city_name" varchar(64),
  "county_name" varchar(64),
  "detail_info" varchar(512),
  "is_default" int2 not null,
  "longitude" varchar(128),
  "latitude" varchar(128),
  "create_time" timestamp,
  constraint "_copy_92" primary key ("id")
);
create index "user_id" on "mall_address" using btree (
  "user_id" asc
);
comment on column "mall_address"."user_id" is '会员id';
comment on column "mall_address"."user_name" is '收货人姓名';
comment on column "mall_address"."mobile" is '手机';
comment on column "mall_address"."postal_code" is '邮编';
comment on column "mall_address"."national_code" is '收货地址国家码';
comment on column "mall_address"."province_name" is '省';
comment on column "mall_address"."city_name" is '市';
comment on column "mall_address"."county_name" is '区';
comment on column "mall_address"."detail_info" is '详细收货地址信息';
comment on column "mall_address"."is_default" is '默认地址 0:否 1:是';
comment on column "mall_address"."longitude" is '经度';
comment on column "mall_address"."latitude" is '纬度';
comment on column "mall_address"."create_time" is '添加时间';
comment on table "mall_address" is '会员地址';

create table "mall_attachment" (
  "id" varchar(32) not null,
  "bussiness_id" varchar(32),
  "name" varchar(512),
  "url" varchar(512),
  "order_sort" int4,
  constraint "_copy_91" primary key ("id")
);
create index "bussiness_id" on "mall_attachment" using btree (
  "bussiness_id" asc
);
comment on column "mall_attachment"."id" is '主键';
comment on column "mall_attachment"."bussiness_id" is '业务表id';
comment on column "mall_attachment"."name" is '名称';
comment on column "mall_attachment"."url" is 'url';
comment on column "mall_attachment"."order_sort" is '排序';
comment on table "mall_attachment" is '附件表';

create table "mall_attribute" (
  "id" varchar(32) not null,
  "name" varchar(64) not null,
  "sort" int4,
  constraint "_copy_90" primary key ("id")
);
comment on column "mall_attribute"."id" is '主键';
comment on column "mall_attribute"."name" is '属性名称';
comment on column "mall_attribute"."sort" is '排序';
comment on table "mall_attribute" is '商品详情参数类型';

create table "mall_bank_type" (
  "id" int8 not null,
  "bank_name" varchar(20) not null,
  "bank_code" varchar(11) not null,
  constraint "_copy_89" primary key ("id")
);
create index "bank_name" on "mall_bank_type" using btree (
  "bank_name" asc
);
comment on column "mall_bank_type"."id" is '主键';
comment on column "mall_bank_type"."bank_name" is '银行名称';
comment on column "mall_bank_type"."bank_code" is '银行id';
comment on table "mall_bank_type" is '银行类型表';

create table "mall_banner" (
  "id" varchar(32) not null,
  "media_type" int2 not null,
  "title" varchar(128),
  "image_url" text,
  "link" varchar(256),
  "content" text,
  "end_time" timestamp,
  "enabled" int2,
  "video_url" varchar(256),
  constraint "_copy_88" primary key ("id")
);
comment on column "mall_banner"."id" is '主键';
comment on column "mall_banner"."media_type" is '类型 0:图片 1:链接 2:文本';
comment on column "mall_banner"."title" is '标题';
comment on column "mall_banner"."image_url" is '图片';
comment on column "mall_banner"."link" is '链接';
comment on column "mall_banner"."content" is '文本';
comment on column "mall_banner"."end_time" is '结束时间';
comment on column "mall_banner"."enabled" is '状态 0:禁用 1:启用';
comment on column "mall_banner"."video_url" is '视频链接';
comment on table "mall_banner" is '首页轮播配置';

create table "mall_brand" (
  "id" varchar(32) not null,
  "name" varchar(255) not null,
  "list_pic_url" varchar(255),
  "app_list_pic_url" varchar(255),
  "simple_desc" varchar(255),
  "sort" int4,
  "is_show" int2,
  "floor_price" numeric(10,2),
  constraint "_copy_87" primary key ("id")
);
create index "is_show" on "mall_brand" using btree (
  "is_show" asc
);
comment on column "mall_brand"."id" is '主键';
comment on column "mall_brand"."name" is '品牌名称';
comment on column "mall_brand"."list_pic_url" is '品牌大图';
comment on column "mall_brand"."app_list_pic_url" is '品牌小图';
comment on column "mall_brand"."simple_desc" is '品牌描述';
comment on column "mall_brand"."sort" is '排序';
comment on column "mall_brand"."is_show" is '显示 0:否 1:是';
comment on column "mall_brand"."floor_price" is '底价';
comment on table "mall_brand" is '品牌制造商';

create table "mall_bulletin" (
  "id" varchar(32) not null,
  "title" varchar(128),
  "content" text,
  "add_time" timestamp,
  "enabled" int2,
  "sort" int4,
  constraint "_copy_86" primary key ("id")
);
comment on column "mall_bulletin"."id" is '主键';
comment on column "mall_bulletin"."title" is '标题';
comment on column "mall_bulletin"."content" is '文本';
comment on column "mall_bulletin"."add_time" is '添加时间';
comment on column "mall_bulletin"."enabled" is '状态 0:禁用 1:启用';
comment on column "mall_bulletin"."sort" is '排序';
comment on table "mall_bulletin" is '商城公告';

create table "mall_cart" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "goods_id" varchar(32),
  "goods_sn" varchar(64),
  "goods_name" varchar(128),
  "sku_id" varchar(32),
  "market_price" numeric(10,2),
  "retail_price" numeric(10,2),
  "number" int4,
  "goods_specifition_name_value" text,
  "checked" int2,
  "list_pic_url" varchar(256),
  "shops_id" varchar(32),
  "create_time" timestamp,
  constraint "_copy_85" primary key ("id")
);
create index "goods_id" on "mall_cart" using btree (
  "goods_id" asc
);
comment on column "mall_cart"."id" is '主键';
comment on column "mall_cart"."user_id" is '会员id';
comment on column "mall_cart"."goods_id" is '商品id';
comment on column "mall_cart"."goods_sn" is '商品编码';
comment on column "mall_cart"."goods_name" is '商品名称';
comment on column "mall_cart"."sku_id" is 'sku_id';
comment on column "mall_cart"."market_price" is '市场价';
comment on column "mall_cart"."retail_price" is '零售价格';
comment on column "mall_cart"."number" is '数量';
comment on column "mall_cart"."goods_specifition_name_value" is '规格属性组成的字符串，用来显示用';
comment on column "mall_cart"."list_pic_url" is '商品图片';
comment on column "mall_cart"."shops_id" is '店铺id';
comment on column "mall_cart"."create_time" is '添加时间';
comment on table "mall_cart" is '购物车';

create table "mall_category" (
  "id" varchar(32) not null,
  "name" varchar(64) not null,
  "level" int2,
  "parent_id" varchar(32),
  "sort" int4,
  "is_show" int2,
  "img_url" varchar(255),
  "icon_url" varchar(255),
  "front_name" varchar(255),
  constraint "_copy_84" primary key ("id")
);
create index "parent_id" on "mall_category" using btree (
  "parent_id" asc
);
comment on column "mall_category"."id" is '主键';
comment on column "mall_category"."name" is '分类名称';
comment on column "mall_category"."level" is '级别：1，2，3';
comment on column "mall_category"."parent_id" is '父节点';
comment on column "mall_category"."sort" is '排序';
comment on column "mall_category"."is_show" is '是否显示 0:否 1:是';
comment on column "mall_category"."img_url" is '分类页面顶部大图';
comment on column "mall_category"."icon_url" is 'icon链接';
comment on column "mall_category"."front_name" is '简介';
comment on table "mall_category" is '商品分类';

create table "mall_channel" (
  "id" varchar(32) not null,
  "name" varchar(64) not null,
  "url" varchar(255) not null,
  "icon_url" varchar(255) not null,
  "sort" int4,
  "status" int2,
  constraint "_copy_83" primary key ("id")
);
comment on column "mall_channel"."id" is '主键';
comment on column "mall_channel"."name" is '名称';
comment on column "mall_channel"."url" is '跳转链接';
comment on column "mall_channel"."icon_url" is 'icon链接';
comment on column "mall_channel"."sort" is '排序';
comment on column "mall_channel"."status" is '状态 0:隐藏 1:显示';
comment on table "mall_channel" is '首页展示分类';

create table "mall_collect" (
  "id" varchar(32) not null,
  "user_id" varchar(32) not null,
  "goods_id" varchar(64),
  "add_time" timestamp,
  constraint "_copy_82" primary key ("id")
);
create index "user_id_collect" on "mall_collect" using btree (
  "user_id" asc
);
create index "goods_id_collect" on "mall_collect" using btree (
  "goods_id" asc
);
comment on column "mall_collect"."id" is '主键';
comment on column "mall_collect"."user_id" is '用户id';
comment on column "mall_collect"."goods_id" is '商品id';
comment on column "mall_collect"."add_time" is '添加时间';
comment on table "mall_collect" is '会员收藏';

create table "mall_comment" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "order_id" varchar(32),
  "goods_id" varchar(32),
  "goods_specifition_name_value" varchar(32),
  "content" text,
  "type" int2 not null,
  "add_time" timestamp,
  "status" int2,
  "eval_level" int2,
  "delivery_level" int2,
  "goods_level" int2,
  constraint "_copy_81" primary key ("id")
);
create index "user_id_comment" on "mall_comment" using btree (
  "user_id" asc
);
create index "goods_id_comment" on "mall_comment" using btree (
  "goods_id" asc
);
comment on column "mall_comment"."id" is '主键';
comment on column "mall_comment"."user_id" is '会员id';
comment on column "mall_comment"."order_id" is '订单id';
comment on column "mall_comment"."goods_id" is '商品id';
comment on column "mall_comment"."goods_specifition_name_value" is '规格属性组成的字符串，用来显示用';
comment on column "mall_comment"."content" is '评价内容，储存为base64编码';
comment on column "mall_comment"."type" is '类型 0:评论的是商品,1:评论的是文章';
comment on column "mall_comment"."add_time" is '记录时间';
comment on column "mall_comment"."status" is '状态 是否被管理员批准显示 0:待审核 1:审核通过';
comment on column "mall_comment"."eval_level" is '评价级别 1-5级 默认 5';
comment on column "mall_comment"."delivery_level" is '配送质量';
comment on column "mall_comment"."goods_level" is '商品服务评价';
comment on table "mall_comment" is '会员评价';

create table "mall_comment_picture" (
  "id" varchar(32) not null,
  "comment_id" varchar(32) not null,
  "pic_url" varchar(255) not null,
  "sort" int4,
  constraint "_copy_80" primary key ("id")
);
create index "comment_id" on "mall_comment_picture" using btree (
  "comment_id" asc
);
comment on column "mall_comment_picture"."id" is '主键';
comment on column "mall_comment_picture"."comment_id" is '评价id';
comment on column "mall_comment_picture"."pic_url" is '评价图片';
comment on column "mall_comment_picture"."sort" is '排序';
comment on table "mall_comment_picture" is '会员评价图片';

create table "mall_coupon" (
  "id" varchar(32) not null,
  "title" varchar(128),
  "coupon_sn" varchar(128),
  "coupon_type" int2,
  "min_price" numeric(10,2),
  "sub_price" numeric(10,2),
  "discount" numeric(3,1),
  "begin_time" timestamp,
  "end_time" timestamp,
  "begin_get_time" timestamp,
  "end_get_time" timestamp,
  "total_count" int4,
  "send_count" int4,
  "limit_type" int2,
  "limit_user" int4,
  "status" int2,
  "sort" int4,
  "is_delete" int2,
  constraint "_copy_79" primary key ("id")
);
comment on column "mall_coupon"."id" is '主键';
comment on column "mall_coupon"."title" is '优惠券标题';
comment on column "mall_coupon"."coupon_sn" is '优惠券编号';
comment on column "mall_coupon"."coupon_type" is '优惠券类型：1:代金券 2:折扣';
comment on column "mall_coupon"."min_price" is '最低消费金额';
comment on column "mall_coupon"."sub_price" is '优惠金额';
comment on column "mall_coupon"."discount" is '折扣率';
comment on column "mall_coupon"."begin_time" is '有效期开始时间';
comment on column "mall_coupon"."end_time" is '有效期结束时间';
comment on column "mall_coupon"."begin_get_time" is '开始领取时间';
comment on column "mall_coupon"."end_get_time" is '结束领取时间';
comment on column "mall_coupon"."total_count" is '优惠券数量';
comment on column "mall_coupon"."send_count" is '已发放数量';
comment on column "mall_coupon"."limit_type" is '指定使用类型 0：全场通用券 1：指定商品 2：指定品牌';
comment on column "mall_coupon"."limit_user" is '每人限制兑换数量';
comment on column "mall_coupon"."status" is '状态 1:可领用 2：过期 3：禁用';
comment on column "mall_coupon"."sort" is '排序';
comment on column "mall_coupon"."is_delete" is '删除状态 0：已删除 1：正常';
comment on table "mall_coupon" is '优惠券列表';

create table "mall_coupon_brand" (
  "id" varchar(32) not null,
  "coupon_id" varchar(32),
  "brand_id" varchar(32),
  constraint "_copy_78" primary key ("id")
);
create index "coupon_id" on "mall_coupon_brand" using btree (
  "coupon_id" asc
);
comment on column "mall_coupon_brand"."id" is '主键';
comment on column "mall_coupon_brand"."coupon_id" is '优惠券id';
comment on column "mall_coupon_brand"."brand_id" is '品牌id';
comment on table "mall_coupon_brand" is '优惠券关联品牌';

create table "mall_coupon_goods" (
  "id" varchar(32) not null,
  "coupon_id" varchar(32),
  "goods_id" varchar(32),
  constraint "_copy_77" primary key ("id")
);
create index "coupon_id_goods" on "mall_coupon_goods" using btree (
  "coupon_id" asc
);
comment on column "mall_coupon_goods"."id" is '主键';
comment on column "mall_coupon_goods"."coupon_id" is '优惠券id';
comment on column "mall_coupon_goods"."goods_id" is '商品id';
comment on table "mall_coupon_goods" is '优惠券关联商品';

create table "mall_dist" (
  "id" int8 not null,
  "user_id" varchar(32) not null,
  "superior_id" int8,
  "name" varchar(255) not null,
  "is_audit" int2 not null,
  "join_time" timestamp not null,
  "amount_available" numeric(10,2) not null,
  "amount_withdrawn" numeric(10,2) not null,
  "amount_total" numeric(10,2) not null,
  "invitation_code" varchar(255) not null,
  "is_delete" int2,
  constraint "_copy_76" primary key ("id")
);
create unique index "user_id_copy_9" on "mall_dist" using btree (
  "user_id" asc
);
create unique index "invitation_code" on "mall_dist" using btree (
  "invitation_code" asc
);
create index "superior_id" on "mall_dist" using btree (
  "superior_id" asc
);
comment on column "mall_dist"."id" is '主键';
comment on column "mall_dist"."user_id" is '会员id';
comment on column "mall_dist"."superior_id" is '上级分销id';
comment on column "mall_dist"."name" is '真实姓名';
comment on column "mall_dist"."is_audit" is '是否已审核';
comment on column "mall_dist"."join_time" is '加入时间';
comment on column "mall_dist"."amount_available" is '待取佣金';
comment on column "mall_dist"."amount_withdrawn" is '已取佣金';
comment on column "mall_dist"."amount_total" is '累计佣金';
comment on column "mall_dist"."invitation_code" is '邀请码';
comment on column "mall_dist"."is_delete" is '删除状态 0：已删除 1：正常';
comment on table "mall_dist" is '分销';

create table "mall_dist_amount_scheduled" (
  "id" varchar(32) not null,
  "dist_order_id" varchar(32) not null,
  "order_id" varchar(32) not null,
  "user_id" varchar(32) not null,
  "income" numeric(10,2),
  "arrive_time" timestamp not null,
  "status" int2 not null,
  constraint "_copy_75" primary key ("id")
);
create index "user_id_amount_scheduled" on "mall_dist_amount_scheduled" using btree (
  "user_id" asc
);
create index "order_id" on "mall_dist_amount_scheduled" using btree (
  "order_id" asc
);
create index "arrive_time" on "mall_dist_amount_scheduled" using btree (
  "arrive_time" asc
);
comment on column "mall_dist_amount_scheduled"."id" is '主键';
comment on column "mall_dist_amount_scheduled"."dist_order_id" is '分销订单id';
comment on column "mall_dist_amount_scheduled"."order_id" is '购物订单id';
comment on column "mall_dist_amount_scheduled"."user_id" is '用户id';
comment on column "mall_dist_amount_scheduled"."income" is '收益';
comment on column "mall_dist_amount_scheduled"."arrive_time" is '到账时间';
comment on column "mall_dist_amount_scheduled"."status" is '定时到账状态 0：未到账 1：已到账 2：购买者退款，失效';
comment on table "mall_dist_amount_scheduled" is '提成定时到账记录';

create table "mall_dist_invitation" (
  "id" int8 not null,
  "user_id" varchar(32) not null,
  "superior_id" int8 not null,
  "create_time" timestamp not null,
  constraint "_copy_74" primary key ("id")
);
create unique index "user_id_copy_7" on "mall_dist_invitation" using btree (
  "user_id" asc
);
comment on column "mall_dist_invitation"."id" is '主键';
comment on column "mall_dist_invitation"."user_id" is '会员id';
comment on column "mall_dist_invitation"."superior_id" is '上级分销id';
comment on column "mall_dist_invitation"."create_time" is '创建时间';
comment on table "mall_dist_invitation" is '分销上级邀请绑定';

create table "mall_dist_order" (
  "id" varchar(32) not null,
  "user_id" varchar(32) not null,
  "buyer_id" varchar(32),
  "order_id" varchar(32),
  "type" varchar(32) not null,
  "income" numeric(10,2),
  "income_time" timestamp,
  "commission_type" varchar(32),
  "commission" numeric(10,8),
  "withdraw_type" varchar(32),
  "enc_bank_no" varchar(32),
  "enc_true_name" varchar(32),
  "bank_code" varchar(32),
  "audit_status" int2 not null,
  "audit_desc" varchar(32),
  "goods_id" varchar(32) not null,
  "sku_id" varchar(32),
  "payment_no" varchar(64),
  constraint "_copy_73" primary key ("id")
);
create index "sort" on "mall_dist_order" using btree (
  "user_id" asc,
  "income_time" asc
);
create index "filter_1" on "mall_dist_order" using btree (
  "user_id" asc,
  "type" asc
);
create index "filter_2" on "mall_dist_order" using btree (
  "user_id" asc,
  "commission_type" asc
);
comment on column "mall_dist_order"."id" is '主键';
comment on column "mall_dist_order"."user_id" is '会员id';
comment on column "mall_dist_order"."buyer_id" is '购买者id';
comment on column "mall_dist_order"."order_id" is '订单id';
comment on column "mall_dist_order"."type" is '订单类型 1：代理提成 2：推广提成 3：佣金提现';
comment on column "mall_dist_order"."income" is '结算收益';
comment on column "mall_dist_order"."income_time" is '结算时间';
comment on column "mall_dist_order"."commission_type" is '提成类型 1：一级分销 2：二级分销 3：一级推广 4：二级推广';
comment on column "mall_dist_order"."commission" is '提成比例 数值范围：0~1';
comment on column "mall_dist_order"."withdraw_type" is '提现类型 1：付款到零钱 2：付款到银行卡';
comment on column "mall_dist_order"."enc_bank_no" is '收款方银行卡号';
comment on column "mall_dist_order"."enc_true_name" is '收款方用户名';
comment on column "mall_dist_order"."bank_code" is '收款方开户行';
comment on column "mall_dist_order"."audit_status" is '审核状态 0：审核中 1：审核通过 2：审核不通过';
comment on column "mall_dist_order"."audit_desc" is '审核说明';
comment on column "mall_dist_order"."goods_id" is '商品id';
comment on column "mall_dist_order"."sku_id" is 'sku_id';
comment on column "mall_dist_order"."payment_no" is '企业付款成功，返回的微信付款单号';
comment on table "mall_dist_order" is '分销订单';

create table "mall_dist_promo" (
  "id" varchar(32) not null,
  "status" int2 not null,
  "cart_id" varchar(32),
  "order_id" varchar(32),
  "goods_id" varchar(32),
  "user_id" varchar(32) not null,
  "sku_id" varchar(32),
  "created_at" timestamp not null,
  "updated_at" timestamp not null,
  constraint "_copy_72" primary key ("id")
);
create index "cart_id" on "mall_dist_promo" using btree (
  "cart_id" asc
);
create index "order_id_dist_promo" on "mall_dist_promo" using btree (
  "order_id" asc
);
create index "goods_id_dist_promo" on "mall_dist_promo" using btree (
  "goods_id" asc
);
create index "user_id_dist_promo" on "mall_dist_promo" using btree (
  "user_id" asc
);
comment on column "mall_dist_promo"."status" is '推广状态 1：加入购物车 2：已生成订单 3：已支付';
comment on column "mall_dist_promo"."cart_id" is '购物车id';
comment on column "mall_dist_promo"."order_id" is '订单id';
comment on column "mall_dist_promo"."goods_id" is '商品id';
comment on column "mall_dist_promo"."user_id" is '推广者用户id';
comment on column "mall_dist_promo"."sku_id" is 'sku_id';
comment on column "mall_dist_promo"."created_at" is '创建时间';
comment on column "mall_dist_promo"."updated_at" is '更新时间';
comment on table "mall_dist_promo" is '推广追踪表';

create table "mall_feedback" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "mobile" varchar(32),
  "feed_type" int2,
  "content" text,
  "status" int2,
  "add_time" timestamp,
  constraint "_copy_71" primary key ("id")
);
create index "user_id_feedback" on "mall_feedback" using btree (
  "user_id" asc
);
comment on column "mall_feedback"."id" is '主键';
comment on column "mall_feedback"."user_id" is '会员id';
comment on column "mall_feedback"."mobile" is '手机';
comment on column "mall_feedback"."feed_type" is '反馈类型 1:商品相关, 2:物流状况, 3:客户服务,4:优惠活动, 5:功能异常, 6:产品建议, 7:其他';
comment on column "mall_feedback"."content" is '详细内容';
comment on column "mall_feedback"."status" is '状态 0:未读 1:已读';
comment on column "mall_feedback"."add_time" is '反馈时间';
comment on table "mall_feedback" is '会员反馈';

create table "mall_footprint" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "goods_id" varchar(32),
  "add_time" timestamp,
  constraint "_copy_70" primary key ("id")
);
comment on column "mall_footprint"."id" is '主键';
comment on column "mall_footprint"."user_id" is '会员id';
comment on column "mall_footprint"."goods_id" is '商品id';
comment on column "mall_footprint"."add_time" is '记录时间';
comment on table "mall_footprint" is '会员足迹';

create table "mall_goods" (
  "id" varchar(32) not null,
  "name" varchar(128) not null,
  "category_id" varchar(32),
  "goods_sn" varchar(64) not null,
  "brand_id" varchar(32),
  "goods_number" int4,
  "is_hot" int2,
  "is_on_sale" int2,
  "is_new" int2,
  "is_limited" int2,
  "is_group" int2,
  "group_number" int4,
  "group_price" numeric(10,2) not null,
  "is_delete" int2,
  "list_pic_url" varchar(256),
  "keywords" varchar(255),
  "goods_brief" varchar(255),
  "unit_price" numeric(10,2),
  "market_price" numeric(10,2),
  "retail_price" numeric(10,2),
  "counter_price" numeric(10,2),
  "min_sell" int4,
  "is_app_exclusive" int2,
  "app_exclusive_price" numeric(10,2),
  "goods_desc" text,
  "sort" int4,
  "promotion_tag" varchar(64),
  "promotion_desc" varchar(256),
  "sales" int4,
  "create_user_id" varchar(32),
  "create_time" timestamp,
  "create_user_org_no" varchar(32),
  "update_user_id" varchar(32),
  "update_time" timestamp,
  "video_url" varchar(256),
  constraint "_copy_69" primary key ("id")
);
create unique index "goods_sn" on "mall_goods" using btree (
  "goods_sn" asc
);
create index "category_id" on "mall_goods" using btree (
  "category_id" asc
);
create index "brand_id" on "mall_goods" using btree (
  "brand_id" asc
);
create index "keywords" on "mall_goods" using btree (
  "keywords" asc
);
create index "is_on_sale" on "mall_goods" using btree (
  "is_on_sale" asc
);
create index "is_delete" on "mall_goods" using btree (
  "is_delete" asc
);
create index "sort_goods" on "mall_goods" using btree (
  "sort" asc
);
comment on column "mall_goods"."id" is '主键';
comment on column "mall_goods"."name" is '名称';
comment on column "mall_goods"."category_id" is '商品类型id';
comment on column "mall_goods"."goods_sn" is '商品编码';
comment on column "mall_goods"."brand_id" is '品牌id';
comment on column "mall_goods"."goods_number" is '商品库存';
comment on column "mall_goods"."is_hot" is '是否热销 0:否 1:是';
comment on column "mall_goods"."is_on_sale" is '是否上架 0:否 1:是';
comment on column "mall_goods"."is_new" is '是否新商品 0:否 1:是';
comment on column "mall_goods"."is_limited" is '是否限购 0:否 1:是';
comment on column "mall_goods"."is_group" is '是否开启拼团 0:否 1:是';
comment on column "mall_goods"."group_number" is '拼团人数(阶梯团取最大阶梯人数)，最大99999';
comment on column "mall_goods"."group_price" is '拼团的最低价格';
comment on column "mall_goods"."is_delete" is '删除状态 0：已删除 1：正常';
comment on column "mall_goods"."list_pic_url" is '列表图';
comment on column "mall_goods"."keywords" is '关键词';
comment on column "mall_goods"."goods_brief" is '简明介绍';
comment on column "mall_goods"."unit_price" is '进价';
comment on column "mall_goods"."market_price" is '市场价';
comment on column "mall_goods"."retail_price" is '零售价格';
comment on column "mall_goods"."counter_price" is '专柜价格';
comment on column "mall_goods"."min_sell" is '起售数量';
comment on column "mall_goods"."is_app_exclusive" is '是否是app专属 0:否 1:是';
comment on column "mall_goods"."app_exclusive_price" is 'app专享价';
comment on column "mall_goods"."goods_desc" is '商品描述';
comment on column "mall_goods"."sort" is '排序';
comment on column "mall_goods"."promotion_tag" is '推广标签';
comment on column "mall_goods"."promotion_desc" is '推广描述';
comment on column "mall_goods"."sales" is '销量';
comment on column "mall_goods"."create_user_id" is '创建人id';
comment on column "mall_goods"."create_time" is '添加时间';
comment on column "mall_goods"."create_user_org_no" is '创建人所属部门';
comment on column "mall_goods"."update_user_id" is '修改人id';
comment on column "mall_goods"."update_time" is '修改时间';
comment on column "mall_goods"."video_url" is '视频链接';
comment on table "mall_goods" is '商品表';

create table "mall_goods_attribute" (
  "id" varchar(32) not null,
  "goods_id" varchar(32),
  "attribute_id" varchar(32),
  "value" varchar(128),
  "order_sort" int4,
  constraint "_copy_68" primary key ("id")
);
create index "goods_id_attribute" on "mall_goods_attribute" using btree (
  "goods_id" asc
);
create index "attribute_id" on "mall_goods_attribute" using btree (
  "attribute_id" asc
);
comment on column "mall_goods_attribute"."id" is '主键';
comment on column "mall_goods_attribute"."goods_id" is '商品id';
comment on column "mall_goods_attribute"."attribute_id" is '属性id';
comment on column "mall_goods_attribute"."value" is '属性值';
comment on column "mall_goods_attribute"."order_sort" is '排序';
comment on table "mall_goods_attribute" is '商品参数列表';

create table "mall_goods_sku" (
  "id" varchar(32) not null,
  "goods_id" varchar(32),
  "goods_sn" varchar(64) not null,
  "goods_number" int4,
  "retail_price" numeric(10,2),
  "market_price" numeric(10,2),
  "list_pic_url" varchar(256),
  "min_sell" int4,
  "group_price" numeric(10,2),
  constraint "_copy_67" primary key ("id")
);
create index "goods_id_goods_sku" on "mall_goods_sku" using btree (
  "goods_id" asc
);
comment on column "mall_goods_sku"."id" is '主键';
comment on column "mall_goods_sku"."goods_id" is '商品id';
comment on column "mall_goods_sku"."goods_sn" is '商品编码';
comment on column "mall_goods_sku"."goods_number" is '商品库存';
comment on column "mall_goods_sku"."retail_price" is '零售价格';
comment on column "mall_goods_sku"."market_price" is '价格';
comment on column "mall_goods_sku"."list_pic_url" is '图片';
comment on column "mall_goods_sku"."min_sell" is '起售数量';
comment on column "mall_goods_sku"."group_price" is '拼团价格';
comment on table "mall_goods_sku" is '商品sku';

create table "mall_goods_specification" (
  "id" varchar(32) not null,
  "goods_id" varchar(32),
  "sku_id" varchar(32),
  "name" varchar(255),
  "value" varchar(64),
  "pic_url" varchar(255),
  constraint "_copy_66" primary key ("id")
);
create index "goods_id_specification" on "mall_goods_specification" using btree (
  "goods_id" asc
);
create index "sku_id" on "mall_goods_specification" using btree (
  "sku_id" asc
);
comment on column "mall_goods_specification"."id" is '主键';
comment on column "mall_goods_specification"."goods_id" is '商品id';
comment on column "mall_goods_specification"."sku_id" is 'sku_id';
comment on column "mall_goods_specification"."name" is 'sku名称';
comment on column "mall_goods_specification"."value" is 'sku值';
comment on column "mall_goods_specification"."pic_url" is '图片';
comment on table "mall_goods_specification" is '商品sku值表';

create table "mall_group_buying_record" (
  "id" varchar(32) not null,
  "goods_id" varchar(32) not null,
  "goods_detail" varchar(255) not null,
  "user_id" varchar(32) not null,
  "nickname" varchar(64),
  "head_img_url" varchar(255),
  "is_leader" int2 not null,
  "leader_id" varchar(32) not null,
  "order_sn" varchar(32) not null,
  "expire_time" timestamp not null,
  "join_number" int4 not null,
  "price" numeric(10,2) not null,
  "status" int2 not null,
  "create_time" timestamp not null,
  "update_time" timestamp,
  "end_time" timestamp,
  constraint "_copy_65" primary key ("id")
);
comment on column "mall_group_buying_record"."id" is '主键';
comment on column "mall_group_buying_record"."goods_id" is '商品id';
comment on column "mall_group_buying_record"."goods_detail" is '商品规格';
comment on column "mall_group_buying_record"."user_id" is '用户id';
comment on column "mall_group_buying_record"."nickname" is '团员昵称';
comment on column "mall_group_buying_record"."head_img_url" is '团员头像';
comment on column "mall_group_buying_record"."is_leader" is '是否团长(开团的人非团购的团长) 1=是 0=否';
comment on column "mall_group_buying_record"."leader_id" is 'pid 如果是团长 则=0';
comment on column "mall_group_buying_record"."order_sn" is '订单编号';
comment on column "mall_group_buying_record"."expire_time" is '到期时间(最迟成团时间，开团时间24小时内)';
comment on column "mall_group_buying_record"."join_number" is '参加人数';
comment on column "mall_group_buying_record"."price" is '拼团的价格';
comment on column "mall_group_buying_record"."status" is '状态 1拼团中 2拼团成功 0拼团失败';
comment on column "mall_group_buying_record"."create_time" is '创建时间';
comment on column "mall_group_buying_record"."update_time" is '修改时间';
comment on column "mall_group_buying_record"."end_time" is '结束时间';
comment on table "mall_group_buying_record" is '商品-拼团记录表';

create table "mall_integral_goods" (
  "id" varchar(32) not null,
  "name" varchar(128) not null,
  "goods_sn" varchar(64) not null,
  "goods_number" int4,
  "is_on_sale" int2,
  "is_delete" int2,
  "list_pic_url" varchar(256),
  "keywords" varchar(255),
  "goods_brief" varchar(255),
  "retail_price" int4,
  "goods_desc" text,
  "sort" int4,
  "sales" int4,
  "create_user_id" varchar(32),
  "create_time" timestamp,
  "create_user_org_no" varchar(32),
  "video_url" varchar(256),
  constraint "_copy_64" primary key ("id")
);
create unique index "goods_sn_copy_1" on "mall_integral_goods" using btree (
  "goods_sn" asc
);
create index "keywords_copy_1" on "mall_integral_goods" using btree (
  "keywords" asc
);
create index "is_on_sale_copy_1" on "mall_integral_goods" using btree (
  "is_on_sale" asc
);
create index "is_delete_copy_1" on "mall_integral_goods" using btree (
  "is_delete" asc
);
create index "sort_copy_1" on "mall_integral_goods" using btree (
  "sort" asc
);
comment on column "mall_integral_goods"."id" is '主键';
comment on column "mall_integral_goods"."name" is '名称';
comment on column "mall_integral_goods"."goods_sn" is '商品编码';
comment on column "mall_integral_goods"."goods_number" is '商品库存';
comment on column "mall_integral_goods"."is_on_sale" is '是否上架 0:否 1:是';
comment on column "mall_integral_goods"."is_delete" is '删除状态 0：已删除 1：正常';
comment on column "mall_integral_goods"."list_pic_url" is '列表图';
comment on column "mall_integral_goods"."keywords" is '关键词';
comment on column "mall_integral_goods"."goods_brief" is '简明介绍';
comment on column "mall_integral_goods"."retail_price" is '积分价格';
comment on column "mall_integral_goods"."goods_desc" is '商品描述';
comment on column "mall_integral_goods"."sort" is '排序';
comment on column "mall_integral_goods"."sales" is '销量';
comment on column "mall_integral_goods"."create_user_id" is '创建人id';
comment on column "mall_integral_goods"."create_time" is '添加时间';
comment on column "mall_integral_goods"."create_user_org_no" is '创建人所属部门';
comment on column "mall_integral_goods"."video_url" is '视频链接';
comment on table "mall_integral_goods" is '积分商品表';

create table "mall_integral_log" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "type" int2 not null,
  "type_detail" int2,
  "number" numeric(10,2) not null,
  "add_time" timestamp,
  constraint "_copy_63" primary key ("id")
);
comment on column "mall_integral_log"."id" is '主键';
comment on column "mall_integral_log"."user_id" is '会员id';
comment on column "mall_integral_log"."type" is '类型：1=收入，2=支出';
comment on column "mall_integral_log"."type_detail" is '类型 1：签到奖励 2：购物奖励 3：抽奖奖励 4：兑换支出';
comment on column "mall_integral_log"."number" is '变动积分数量';
comment on column "mall_integral_log"."add_time" is '添加时间';
comment on table "mall_integral_log" is '积分变动记录';

create table "mall_issue" (
  "id" varchar(32) not null,
  "question" text,
  "answer" text,
  constraint "_copy_62" primary key ("id")
);
comment on column "mall_issue"."id" is '主键';
comment on column "mall_issue"."question" is '问题';
comment on column "mall_issue"."answer" is '回答';
comment on table "mall_issue" is '商品问答';

create table "mall_keywords" (
  "id" varchar(32) not null,
  "keyword" varchar(128) not null,
  "type" int2,
  "is_hot" int2,
  "is_default" int2,
  "is_show" int2,
  "scheme_url" varchar(255),
  "sort" int4,
  constraint "_copy_61" primary key ("id")
);
create index "keyword" on "mall_keywords" using btree (
  "keyword" asc
);
create index "is_show_copy_1" on "mall_keywords" using btree (
  "is_show" asc
);
comment on column "mall_keywords"."id" is '主键';
comment on column "mall_keywords"."keyword" is '关键词';
comment on column "mall_keywords"."type" is '场景 0:小程序 1:公众号 2:app 3:pc';
comment on column "mall_keywords"."is_hot" is '热搜 0:否 1:是';
comment on column "mall_keywords"."is_default" is '默认 0:否 1:是';
comment on column "mall_keywords"."is_show" is '显示 0:否 1:是';
comment on column "mall_keywords"."scheme_url" is '跳转链接';
comment on column "mall_keywords"."sort" is '排序';
comment on table "mall_keywords" is '搜索关键词';

create table "mall_order" (
  "id" varchar(32) not null,
  "from_type" int2,
  "order_type" int2,
  "order_sn" varchar(32),
  "user_id" varchar(32),
  "order_status" int2,
  "shipping_status" int2,
  "pay_status" int2,
  "consignee" varchar(128),
  "country" varchar(64),
  "province" varchar(64),
  "city" varchar(64),
  "district" varchar(64),
  "address" varchar(255),
  "mobile" varchar(32),
  "postscript" varchar(512),
  "shipping_id" varchar(32),
  "shipping_name" varchar(64),
  "shipping_code" varchar(64),
  "shipping_no" varchar(64),
  "shipping_fee" numeric(10,2),
  "prepay_id" varchar(64),
  "actual_price" numeric(10,2),
  "integral_money" numeric(10,2),
  "order_price" numeric(10,2),
  "add_time" timestamp,
  "confirm_time" timestamp,
  "pay_type" int2,
  "pay_time" timestamp,
  "coupon_id" varchar(32),
  "coupon_price" numeric(10,2),
  "parent_id" varchar(32),
  "shops_id" varchar(32),
  "goods_id" varchar(32),
  "expire_time" timestamp,
  "postal_code" varchar(64),
  "group_id" varchar(32),
  constraint "_copy_60" primary key ("id")
);
create unique index "order_sn" on "mall_order" using btree (
  "order_sn" asc
);
create index "user_id_copy_4" on "mall_order" using btree (
  "user_id" asc
);
create index "order_status" on "mall_order" using btree (
  "order_status" asc
);
create index "shipping_status" on "mall_order" using btree (
  "shipping_status" asc
);
create index "pay_status" on "mall_order" using btree (
  "pay_status" asc
);
create index "consignee" on "mall_order" using btree (
  "consignee" asc
);
create index "prepay_id" on "mall_order" using btree (
  "prepay_id" asc
);
create index "parent_id_copy_1" on "mall_order" using btree (
  "parent_id" asc
);
comment on column "mall_order"."id" is '主键';
comment on column "mall_order"."order_type" is '订单类型 1：普通订单 2：店铺订单 3:秒杀订单';
comment on column "mall_order"."order_sn" is '订单编号';
comment on column "mall_order"."user_id" is '会员id';
comment on column "mall_order"."order_status" is '订单状态 0:订单创建成功等待付款 101:订单已取消 102:订单已删除 201:订单已付款,等待发货 300:订单已发货 301:用户确认收货 401:没有发货，退款　402:已收货，退款退货';
comment on column "mall_order"."shipping_status" is '发货状态 商品配送情况;1:未发货 2:已发货 3:已收货 4:退货';
comment on column "mall_order"."pay_status" is '付款状态 1:未付款 2:付款中 3:已付款 4:退款';
comment on column "mall_order"."consignee" is '收货人';
comment on column "mall_order"."country" is '国家';
comment on column "mall_order"."province" is '省';
comment on column "mall_order"."city" is '市';
comment on column "mall_order"."district" is '地区';
comment on column "mall_order"."address" is '详细地址';
comment on column "mall_order"."mobile" is '手机号';
comment on column "mall_order"."postscript" is '留言';
comment on column "mall_order"."shipping_id" is '快递公司id';
comment on column "mall_order"."shipping_name" is '快递公司名称';
comment on column "mall_order"."shipping_code" is '快递公司code';
comment on column "mall_order"."shipping_no" is '快递单号';
comment on column "mall_order"."shipping_fee" is '快递费用';
comment on column "mall_order"."prepay_id" is '支付prepay_id';
comment on column "mall_order"."actual_price" is '实际支付的金额';
comment on column "mall_order"."integral_money" is '积分抵扣金额';
comment on column "mall_order"."order_price" is '订单总价';
comment on column "mall_order"."add_time" is '下单时间';
comment on column "mall_order"."confirm_time" is '确认时间';
comment on column "mall_order"."pay_type" is '支付方式 1：微信支付  2：余额支付';
comment on column "mall_order"."pay_time" is '付款时间';
comment on column "mall_order"."coupon_id" is '使用的优惠券id';
comment on column "mall_order"."coupon_price" is '优惠价格';
comment on column "mall_order"."parent_id" is '父级订单id';
comment on column "mall_order"."shops_id" is '店铺id';
comment on column "mall_order"."goods_id" is '秒杀商品';
comment on column "mall_order"."expire_time" is '订单过期时间';
comment on column "mall_order"."group_id" is '团id';
comment on table "mall_order" is '订单表';

create table "mall_order_goods" (
  "id" varchar(32) not null,
  "order_id" varchar(32) not null,
  "goods_id" varchar(32) not null,
  "goods_name" varchar(128) not null,
  "goods_sn" varchar(64) not null,
  "sku_id" varchar(32),
  "number" int4,
  "market_price" numeric(10,2),
  "retail_price" numeric(10,2) not null,
  "goods_specifition_name_value" text,
  "list_pic_url" varchar(255),
  constraint "_copy_59" primary key ("id")
);
create index "order_id_copy_1" on "mall_order_goods" using btree (
  "order_id" asc
);
comment on column "mall_order_goods"."id" is '主键';
comment on column "mall_order_goods"."order_id" is '订单id';
comment on column "mall_order_goods"."goods_id" is '商品id';
comment on column "mall_order_goods"."goods_name" is '商品名称';
comment on column "mall_order_goods"."goods_sn" is '商品编码';
comment on column "mall_order_goods"."sku_id" is 'sku_id';
comment on column "mall_order_goods"."number" is '商品数量';
comment on column "mall_order_goods"."market_price" is '市场价';
comment on column "mall_order_goods"."retail_price" is '零售价格';
comment on column "mall_order_goods"."goods_specifition_name_value" is '商品规格详情';
comment on column "mall_order_goods"."list_pic_url" is '图片链接';
comment on table "mall_order_goods" is '订单详情';

create table "mall_order_refund" (
  "id" varchar(32) not null,
  "order_sn" varchar(32),
  "saleservice_id" varchar(32),
  "user_id" varchar(32),
  "refund_type" int2,
  "refund_status" int2,
  "refund_time" timestamp,
  "refund_money" numeric(8,2),
  "refund_reason" varchar(255),
  "approver" varchar(32),
  "approval_time" timestamp,
  "approval_remark" varchar(255),
  constraint "_copy_58" primary key ("id")
);
comment on column "mall_order_refund"."id" is '主键';
comment on column "mall_order_refund"."order_sn" is '订单编码';
comment on column "mall_order_refund"."saleservice_id" is '售后单id';
comment on column "mall_order_refund"."user_id" is '用户id';
comment on column "mall_order_refund"."refund_type" is '退款类型 1用户退款 2系统退款 3售后退款';
comment on column "mall_order_refund"."refund_status" is ' 退款状态 1申请中 2退款成功 3已拒绝';
comment on column "mall_order_refund"."refund_time" is '退款时间';
comment on column "mall_order_refund"."refund_money" is '退款金额';
comment on column "mall_order_refund"."refund_reason" is '退款原因';
comment on column "mall_order_refund"."approver" is '审核人';
comment on column "mall_order_refund"."approval_time" is '审核时间';
comment on column "mall_order_refund"."approval_remark" is '审核备注';
comment on table "mall_order_refund" is '退款记录';

create table "mall_order_saleservice" (
  "id" varchar(32) not null,
  "order_sn" varchar(32),
  "user_id" varchar(32),
  "saleservice_sn" varchar(255),
  "reason" varchar(100),
  "amount" numeric(10,2),
  "status" int2,
  "create_time" timestamp,
  "handle_time" timestamp,
  "refund_time" timestamp,
  "handle_reason" varchar(200),
  "remark" varchar(2000),
  constraint "_copy_57" primary key ("id")
);
comment on column "mall_order_saleservice"."id" is '主键';
comment on column "mall_order_saleservice"."order_sn" is '订单编号';
comment on column "mall_order_saleservice"."user_id" is '申请人id';
comment on column "mall_order_saleservice"."saleservice_sn" is '退款单编号';
comment on column "mall_order_saleservice"."reason" is '申请退款原因';
comment on column "mall_order_saleservice"."amount" is '申请退款金额';
comment on column "mall_order_saleservice"."status" is '状态1：已申请 2：已审核 3：已退款 4：已驳回';
comment on column "mall_order_saleservice"."create_time" is '申请时间';
comment on column "mall_order_saleservice"."handle_time" is '审核时间';
comment on column "mall_order_saleservice"."refund_time" is '退款时间';
comment on column "mall_order_saleservice"."handle_reason" is '审核原因';
comment on column "mall_order_saleservice"."remark" is '备注';
comment on table "mall_order_saleservice" is '申请退款';

create table "mall_pay_face_to_face" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "from_type" int2,
  "order_sn" varchar(32),
  "pay_status" int2,
  "actual_price" numeric(10,2),
  "add_time" timestamp,
  "pay_time" timestamp,
  "shops_id" varchar(32),
  constraint "_copy_56" primary key ("id")
);
create unique index "order_sn_copy_1" on "mall_pay_face_to_face" using btree (
  "order_sn" asc
);
create index "user_id_copy_3" on "mall_pay_face_to_face" using btree (
  "user_id" asc
);
comment on column "mall_pay_face_to_face"."id" is '主键';
comment on column "mall_pay_face_to_face"."user_id" is '会员id';
comment on column "mall_pay_face_to_face"."from_type" is '用户下单来源类型 1:微信小程序 2:微信公众号 3:app 4:h5 5:支付宝小程序 6:qq小程序';
comment on column "mall_pay_face_to_face"."order_sn" is '订单编号';
comment on column "mall_pay_face_to_face"."pay_status" is '付款状态 1:未付款 2:付款中 3:已付款';
comment on column "mall_pay_face_to_face"."actual_price" is '实际支付的金额';
comment on column "mall_pay_face_to_face"."add_time" is '下单时间';
comment on column "mall_pay_face_to_face"."pay_time" is '付款时间';
comment on column "mall_pay_face_to_face"."shops_id" is '店铺id';
comment on table "mall_pay_face_to_face" is '当面付记录';

create table "mall_room" (
  "roomid" int4 not null,
  "name" varchar(64) not null,
  "cover_img" varchar(128),
  "share_img" varchar(128),
  "start_time" int4,
  "end_time" int4,
  "anchor_name" varchar(255),
  "sub_anchor_wechat" varchar(255),
  "creater_wechat" varchar(255),
  "feeds_img" varchar(255),
  "is_feeds_public" int2,
  "close_replay" int2,
  "close_share" int2,
  "close_kf" int2,
  "live_status" varchar(255),
  constraint "_copy_55" primary key ("roomid")
);
comment on column "mall_room"."roomid" is '房间 id';
comment on column "mall_room"."name" is '房间名';
comment on column "mall_room"."cover_img" is '直播间背景图，图片规则：建议像素1080*1920，大小不超过2m';
comment on column "mall_room"."share_img" is '直播间分享图，图片规则：建议像素800*640，大小不超过1m';
comment on column "mall_room"."start_time" is '直播计划开始时间，列表按照 start_time 降序排列';
comment on column "mall_room"."end_time" is '直播计划结束时间';
comment on column "mall_room"."anchor_name" is '主播名';
comment on column "mall_room"."sub_anchor_wechat" is '主播副号微信号';
comment on column "mall_room"."creater_wechat" is '创建者微信号';
comment on column "mall_room"."feeds_img" is '购物直播频道封面图';
comment on column "mall_room"."is_feeds_public" is '是否开启官方收录 【1: 开启，0：关闭】，默认开启收录';
comment on column "mall_room"."close_replay" is '是否关闭回放 【0：开启，1：关闭】默认关闭回放';
comment on column "mall_room"."close_share" is '是否关闭分享 【0：开启，1：关闭】默认开启分享（直播开始后不允许修改）';
comment on column "mall_room"."close_kf" is '是否关闭客服 【0：开启，1：关闭】 默认关闭客服';
comment on column "mall_room"."live_status" is '直播状态 101: 直播中, 102: 未开始, 103: 已结束, 104: 禁播, 105: 暂停中, 106: 异常, 107: 已过期';
comment on table "mall_room" is '直播房间信息';

create table "mall_room_all_goods" (
  "goods_id" int4 not null,
  "cover_img_url" varchar(128),
  "name" varchar(64) not null,
  "price_type" int2,
  "price" varchar(64),
  "price2" varchar(64),
  "url" varchar(128),
  "audit_status" int2,
  "third_party_tag" int2,
  "audit_id" int4,
  constraint "_copy_54" primary key ("goods_id")
);
comment on column "mall_room_all_goods"."goods_id" is '商品 id';
comment on column "mall_room_all_goods"."cover_img_url" is '商品图片';
comment on column "mall_room_all_goods"."name" is '商品名';
comment on column "mall_room_all_goods"."price_type" is '1:一口价，此时读price字段; 2:价格区间，此时price字段为左边界，price2字段为右边界; 3:折扣价，此时price字段为原价，price2字段为现价；';
comment on column "mall_room_all_goods"."price" is '商品价格';
comment on column "mall_room_all_goods"."price2" is '商品价格';
comment on column "mall_room_all_goods"."url" is '商品url';
comment on column "mall_room_all_goods"."audit_status" is '0：未审核，1：审核中，2:审核通过，3审核失败';
comment on column "mall_room_all_goods"."third_party_tag" is '1, 2：表示是为api添加商品，否则是在mp添加商品';
comment on column "mall_room_all_goods"."audit_id" is '审核单id';
comment on table "mall_room_all_goods" is '直播接口所有商品信息';

create table "mall_room_goods" (
  "id" varchar(32) not null,
  "roomid" int4 not null,
  "cover_img" varchar(128),
  "name" varchar(64) not null,
  "url" varchar(128),
  "price" int4,
  "price2" varchar(64),
  constraint "_copy_53" primary key ("id")
);
comment on column "mall_room_goods"."id" is '主键';
comment on column "mall_room_goods"."roomid" is '房间 id';
comment on column "mall_room_goods"."cover_img" is '直播间背景图，图片规则：建议像素1080*1920，大小不超过2m';
comment on column "mall_room_goods"."name" is '商品名';
comment on column "mall_room_goods"."url" is '商品图片url';
comment on column "mall_room_goods"."price" is '商品价格';
comment on column "mall_room_goods"."price2" is '商品价格2';
comment on table "mall_room_goods" is '直播房间商品信息';

create table "mall_room_media" (
  "roomid" int4 not null,
  "media_url" varchar(216),
  "expire_time" timestamp,
  "create_time" timestamp
);
comment on column "mall_room_media"."roomid" is '房间 id';
comment on column "mall_room_media"."media_url" is '回放视频 url';
comment on column "mall_room_media"."expire_time" is '直播计划开始时间，列表按照 start_time 降序排列';
comment on column "mall_room_media"."create_time" is '创建时间';
comment on table "mall_room_media" is '直播房间回放视频';

create table "mall_search_history" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "keyword" varchar(32),
  "search_from" int2,
  "add_time" timestamp,
  constraint "_copy_52" primary key ("id")
);
comment on column "mall_search_history"."id" is '主键';
comment on column "mall_search_history"."user_id" is '会员id';
comment on column "mall_search_history"."keyword" is '关键词';
comment on column "mall_search_history"."search_from" is '搜索来源 0:小程序 1:公众号 2:app 3:pc';
comment on column "mall_search_history"."add_time" is '搜索时间';
comment on table "mall_search_history" is '搜索历史';

create table "mall_seckill" (
  "id" varchar(32) not null,
  "goods_id" varchar(32),
  "name" varchar(128) not null,
  "list_pic_url" varchar(255),
  "price" numeric(10,2),
  "stock" int4 not null,
  "start_time" timestamp not null,
  "end_time" timestamp not null,
  "create_time" timestamp,
  "version" int4,
  constraint "_copy_51" primary key ("id")
);
create index "start_time" on "mall_seckill" using btree (
  "start_time" asc
);
create index "end_time" on "mall_seckill" using btree (
  "end_time" asc
);
create index "create_time" on "mall_seckill" using btree (
  "create_time" asc
);
comment on column "mall_seckill"."id" is '主键';
comment on column "mall_seckill"."goods_id" is '商品id';
comment on column "mall_seckill"."name" is '秒杀名称';
comment on column "mall_seckill"."list_pic_url" is '图片链接';
comment on column "mall_seckill"."price" is '秒杀金额';
comment on column "mall_seckill"."stock" is '秒杀库存';
comment on column "mall_seckill"."start_time" is '秒杀开启时间';
comment on column "mall_seckill"."end_time" is '秒杀结束时间';
comment on column "mall_seckill"."create_time" is '创建时间';
comment on column "mall_seckill"."version" is '版本号';
comment on table "mall_seckill" is '秒杀库存表';

create table "mall_shipping" (
  "id" int4 not null,
  "code" varchar(64),
  "name" varchar(128),
  "status" int2,
  constraint "_copy_50" primary key ("id")
);
comment on column "mall_shipping"."id" is '主键';
comment on column "mall_shipping"."code" is '快递公司编码';
comment on column "mall_shipping"."name" is '快递公司名称';
comment on column "mall_shipping"."status" is '状态 0：禁用 1：正常';
comment on table "mall_shipping" is '快递公司配置';

create table "mall_shops" (
  "id" varchar(32) not null,
  "qr_code" varchar(256),
  "name" varchar(128) not null,
  "shops_sn" varchar(128) not null,
  "img_url" varchar(128) not null,
  "user_id" varchar(32),
  "work_time" varchar(128),
  "longitude" varchar(128),
  "latitude" varchar(128),
  "details" varchar(256),
  "telephone" varchar(64),
  "delete_status" int2,
  "shop_desc" text,
  "sort" int2,
  "create_user_id" varchar(32),
  "create_user_org_no" varchar(32),
  "create_time" timestamp,
  constraint "_copy_49" primary key ("id")
);
comment on column "mall_shops"."id" is '主键';
comment on column "mall_shops"."qr_code" is '店铺小程序码';
comment on column "mall_shops"."name" is '店铺名字';
comment on column "mall_shops"."shops_sn" is '店铺编号';
comment on column "mall_shops"."img_url" is '店铺图片';
comment on column "mall_shops"."user_id" is '店铺管理员';
comment on column "mall_shops"."work_time" is '营业时间';
comment on column "mall_shops"."longitude" is '经度';
comment on column "mall_shops"."latitude" is '纬度';
comment on column "mall_shops"."details" is '详细位置';
comment on column "mall_shops"."telephone" is '联系电话';
comment on column "mall_shops"."delete_status" is '状态 0：已删除 1：正常';
comment on column "mall_shops"."shop_desc" is '描述';
comment on column "mall_shops"."sort" is '排序';
comment on column "mall_shops"."create_user_id" is '创建人';
comment on column "mall_shops"."create_user_org_no" is '创建人所属机构';
comment on column "mall_shops"."create_time" is '创建时间';
comment on table "mall_shops" is '店铺';

create table "mall_shops_category" (
  "id" varchar(32) not null,
  "name" varchar(32) not null,
  "shops_id" varchar(32) not null,
  "status" int2,
  "sort" int2,
  "icon" varchar(256),
  "create_user_id" varchar(32),
  "create_user_org_no" varchar(32),
  "create_time" timestamp,
  constraint "_copy_48" primary key ("id")
);
create index "shops_id" on "mall_shops_category" using btree (
  "shops_id" asc
);
comment on column "mall_shops_category"."id" is '主键';
comment on column "mall_shops_category"."name" is '分类名称';
comment on column "mall_shops_category"."shops_id" is '店铺id';
comment on column "mall_shops_category"."status" is '状态 0:隐藏 1:显示';
comment on column "mall_shops_category"."sort" is '排序';
comment on column "mall_shops_category"."icon" is '图标';
comment on column "mall_shops_category"."create_user_id" is '创建者id';
comment on column "mall_shops_category"."create_user_org_no" is '创建人所属机构';
comment on column "mall_shops_category"."create_time" is '创建时间';
comment on table "mall_shops_category" is '店铺商品分类';

create table "mall_shops_goods" (
  "id" varchar(32) not null,
  "shops_id" varchar(32),
  "shops_category_id" varchar(32),
  "goods_id" varchar(32),
  "retail_price" numeric(10,2),
  "goods_number" int4,
  "sort" int2,
  constraint "_copy_47" primary key ("id")
);
create index "shops_id_copy_3" on "mall_shops_goods" using btree (
  "shops_id" asc
);
create index "shops_category_id" on "mall_shops_goods" using btree (
  "shops_category_id" asc
);
comment on column "mall_shops_goods"."id" is '主键';
comment on column "mall_shops_goods"."shops_id" is '店铺id';
comment on column "mall_shops_goods"."shops_category_id" is '商品分类id';
comment on column "mall_shops_goods"."goods_id" is '商品id';
comment on column "mall_shops_goods"."retail_price" is '零售价格';
comment on column "mall_shops_goods"."goods_number" is '店铺商品库存';
comment on column "mall_shops_goods"."sort" is '排序';
comment on table "mall_shops_goods" is '店铺与商品关联关系';

create table "mall_specification" (
  "id" varchar(32) not null,
  "goods_id" varchar(32),
  "name" varchar(64),
  "sort" int2,
  constraint "_copy_46" primary key ("id")
);
comment on column "mall_specification"."id" is '主键';
comment on column "mall_specification"."goods_id" is '商品id';
comment on column "mall_specification"."name" is 'sku键名称';
comment on column "mall_specification"."sort" is '排序';
comment on table "mall_specification" is '商品sku键表';

create table "mall_template_conf" (
  "id" varchar(32) not null,
  "template_type" int2,
  "template_id" varchar(64),
  constraint "_copy_45" primary key ("id")
);
comment on column "mall_template_conf"."id" is '主键';
comment on column "mall_template_conf"."template_type" is '模板类型 0:新订单提醒	 1:下单成功通知 2:订单评价提醒 3:退款 4:秒杀成功通知 5:订单配送通知';
comment on column "mall_template_conf"."template_id" is '推送模板id';
comment on table "mall_template_conf" is '微信订阅消息';

create table "mall_topic" (
  "id" varchar(32) not null,
  "title" varchar(255) not null,
  "content" text,
  "avatar" varchar(255) not null,
  "item_pic_url" varchar(255) not null,
  "subtitle" varchar(255) not null,
  "topic_category_id" varchar(32) not null,
  "price_info" numeric(10,2) not null,
  "read_count" int8 not null,
  "scene_pic_url" varchar(255) not null,
  "topic_template_id" varchar(32) not null,
  "topic_tag_id" varchar(32) not null,
  "author" varchar(255),
  "create_time" timestamp
);
create index "topic_id" on "mall_topic" using btree (
  "id" asc
);
comment on column "mall_topic"."id" is '主键';
comment on column "mall_topic"."title" is '活动主题';
comment on column "mall_topic"."content" is '活动内容';
comment on column "mall_topic"."avatar" is '化名';
comment on column "mall_topic"."item_pic_url" is '活动条例图片';
comment on column "mall_topic"."subtitle" is '子标题';
comment on column "mall_topic"."topic_category_id" is '活动类别';
comment on column "mall_topic"."price_info" is '活动价格';
comment on column "mall_topic"."scene_pic_url" is '场景图片链接';
comment on column "mall_topic"."topic_template_id" is '活动模板id';
comment on column "mall_topic"."topic_tag_id" is '活动标签id';
comment on column "mall_topic"."author" is '作者';
comment on column "mall_topic"."create_time" is '创建时间';
comment on table "mall_topic" is '活动';

create table "mall_topic_category" (
  "id" varchar(32) not null,
  "title" varchar(255) not null,
  "pic_url" varchar(255) not null,
  constraint "_copy_44" primary key ("id")
);
comment on column "mall_topic_category"."id" is '主键';
comment on column "mall_topic_category"."title" is '活动类别主题';
comment on column "mall_topic_category"."pic_url" is '活动类别图片链接';
comment on table "mall_topic_category" is '活动分类';

create table "mall_user" (
  "id" varchar(32) not null,
  "user_name" varchar(128) not null,
  "password" varchar(128),
  "gender" int2,
  "birthday" timestamp,
  "register_time" timestamp,
  "last_login_time" timestamp,
  "last_login_ip" varchar(32),
  "user_level_id" varchar(32),
  "nickname" varchar(60),
  "mobile" varchar(256),
  "register_ip" varchar(64),
  "head_img_url" varchar(255),
  "ali_user_id" varchar(64),
  "open_id" varchar(64),
  "mp_open_id" varchar(64),
  "qq_open_id" varchar(64),
  "union_id" varchar(128),
  "subscribe" int2,
  "subscribe_time" varchar(32),
  "sign_all_integral" numeric(10,2),
  "sign_used_integral" numeric(10,2),
  "balance" numeric(10,2),
  "integral" numeric(10,2)
);
create unique index "mall_user_id_uindex" on "mall_user" using btree (
  "id" asc
);
create index "open_id" on "mall_user" using btree (
  "open_id" asc
);
comment on column "mall_user"."id" is '用户id';
comment on column "mall_user"."user_name" is '用户名';
comment on column "mall_user"."password" is '密码';
comment on column "mall_user"."gender" is '用户的性别（1是男性，2是女性，0是未知）';
comment on column "mall_user"."birthday" is '生日';
comment on column "mall_user"."register_time" is '注册时间';
comment on column "mall_user"."last_login_time" is '最后登录时间';
comment on column "mall_user"."last_login_ip" is '最后登录ip';
comment on column "mall_user"."user_level_id" is '会员等级id';
comment on column "mall_user"."nickname" is '微信昵称';
comment on column "mall_user"."mobile" is '手机号';
comment on column "mall_user"."register_ip" is '注册ip';
comment on column "mall_user"."head_img_url" is '用户头像';
comment on column "mall_user"."ali_user_id" is '支付宝用户标识';
comment on column "mall_user"."open_id" is '用户的标识';
comment on column "mall_user"."mp_open_id" is '公众号用户的标识';
comment on column "mall_user"."qq_open_id" is 'qq用户标识';
comment on column "mall_user"."union_id" is '用户唯一标识';
comment on column "mall_user"."subscribe" is '公众号关注状态（1是关注，0是未关注），未关注时获取不到其余信息';
comment on column "mall_user"."subscribe_time" is '用户关注公众号时间，为时间戳。如果用户曾多次关注，则取最后关注时间';
comment on column "mall_user"."sign_all_integral" is '签到、购物获得总积分';
comment on column "mall_user"."sign_used_integral" is '已兑换积分';
comment on column "mall_user"."balance" is '余额';
comment on column "mall_user"."integral" is '积分';
comment on table "mall_user" is '会员';

create table "mall_user_bank_card" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "card_name" varchar(32),
  "card_number" varchar(32),
  "card_type" varchar(32),
  "bank_type_id" int8 not null,
  "card_status" int2 not null,
  "bound_at" timestamp not null,
  constraint "_copy_43" primary key ("id")
);
create index "user_id_copy_2" on "mall_user_bank_card" using btree (
  "user_id" asc
);
comment on column "mall_user_bank_card"."id" is '主键';
comment on column "mall_user_bank_card"."user_id" is '会员id';
comment on column "mall_user_bank_card"."card_name" is '收款人姓名';
comment on column "mall_user_bank_card"."card_number" is '银行卡号';
comment on column "mall_user_bank_card"."card_type" is '卡类型';
comment on column "mall_user_bank_card"."bank_type_id" is '银行id';
comment on column "mall_user_bank_card"."card_status" is '1：已绑定 2：已解绑';
comment on column "mall_user_bank_card"."bound_at" is '绑定时间';
comment on table "mall_user_bank_card" is '用户银行卡表';

create table "mall_user_coupon" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "coupon_id" varchar(32),
  "add_time" timestamp,
  "type" int2,
  "status" int2,
  "used_time" timestamp,
  "order_id" varchar(32),
  constraint "_copy_42" primary key ("id")
);
create index "user_id_copy_1" on "mall_user_coupon" using btree (
  "user_id" asc
);
create index "coupon_id_copy_1" on "mall_user_coupon" using btree (
  "coupon_id" asc
);
create index "status" on "mall_user_coupon" using btree (
  "status" asc
);
comment on column "mall_user_coupon"."id" is '主键';
comment on column "mall_user_coupon"."user_id" is '会员id';
comment on column "mall_user_coupon"."coupon_id" is '优惠券id';
comment on column "mall_user_coupon"."add_time" is '领用时间';
comment on column "mall_user_coupon"."type" is '领取类型 0:平台发放 1:自动发放 2:领券中心领取';
comment on column "mall_user_coupon"."status" is '状态 0:未使用 1:已使用 2：过期';
comment on column "mall_user_coupon"."used_time" is '使用时间';
comment on column "mall_user_coupon"."order_id" is '使用的订单id';
comment on table "mall_user_coupon" is '会员优惠券';

create table "mall_user_level" (
  "id" varchar(32) not null,
  "name" varchar(64) not null,
  "money" numeric(10,2),
  "discount" numeric(10,2),
  "image_url" varchar(128),
  "description" varchar(255) not null,
  constraint "_copy_41" primary key ("id")
);
comment on column "mall_user_level"."id" is '主键';
comment on column "mall_user_level"."name" is '等级名称';
comment on column "mall_user_level"."money" is '会员完成订单金额满足则升级';
comment on column "mall_user_level"."discount" is '折扣';
comment on column "mall_user_level"."image_url" is '会员等级图片';
comment on column "mall_user_level"."description" is '描述';
comment on table "mall_user_level" is '会员等级管理';

create table "mall_user_sign_record" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "sign_time" timestamp,
  "sign_integral" numeric(10,2),
  constraint "_copy_40" primary key ("id")
);
comment on column "mall_user_sign_record"."id" is '主键';
comment on column "mall_user_sign_record"."user_id" is '会员id';
comment on column "mall_user_sign_record"."sign_time" is '签到时间';
comment on column "mall_user_sign_record"."sign_integral" is '本次签到获得积分';
comment on table "mall_user_sign_record" is '用户签到记录';

create table "schedule_job" (
  "job_id" varchar(32) not null,
  "bean_name" varchar(200),
  "method_name" varchar(100),
  "params" varchar(2000),
  "cron_expression" varchar(100),
  "status" int2,
  "remark" varchar(255),
  "create_time" timestamp,
  constraint "_copy_28" primary key ("job_id")
);
comment on column "schedule_job"."job_id" is '任务id';
comment on column "schedule_job"."bean_name" is 'spring bean名称';
comment on column "schedule_job"."method_name" is '方法名';
comment on column "schedule_job"."params" is '参数';
comment on column "schedule_job"."cron_expression" is 'cron表达式';
comment on column "schedule_job"."status" is '任务状态  0：正常  1：暂停';
comment on column "schedule_job"."remark" is '备注';
comment on column "schedule_job"."create_time" is '创建时间';
comment on table "schedule_job" is '定时任务';

create table "schedule_job_log" (
  "log_id" varchar(32) not null,
  "job_id" varchar(32) not null,
  "bean_name" varchar(200),
  "method_name" varchar(100),
  "params" varchar(2000),
  "status" int2 not null,
  "error" varchar(2000),
  "times" int4 not null,
  "create_time" timestamp,
  constraint "_copy_27" primary key ("log_id")
);
create index "job_id" on "schedule_job_log" using btree (
  "job_id" asc
);
comment on column "schedule_job_log"."log_id" is '任务日志id';
comment on column "schedule_job_log"."job_id" is '任务id';
comment on column "schedule_job_log"."bean_name" is 'spring bean名称';
comment on column "schedule_job_log"."method_name" is '方法名';
comment on column "schedule_job_log"."params" is '参数';
comment on column "schedule_job_log"."status" is '任务状态    0：成功    1：失败';
comment on column "schedule_job_log"."error" is '失败信息';
comment on column "schedule_job_log"."times" is '耗时(单位：毫秒)';
comment on column "schedule_job_log"."create_time" is '创建时间';
comment on table "schedule_job_log" is '定时任务日志';

create table "sys_captcha" (
  "uuid" char(36) not null,
  "code" varchar(6) not null,
  "expire_time" timestamp,
  constraint "_copy_26" primary key ("uuid")
);
comment on column "sys_captcha"."uuid" is 'uuid';
comment on column "sys_captcha"."code" is '验证码';
comment on column "sys_captcha"."expire_time" is '过期时间';
comment on table "sys_captcha" is '系统验证码';

create table "sys_config" (
  "id" varchar(32) not null,
  "param_key" varchar(50),
  "param_value" text,
  "status" int2,
  "remark" varchar(500),
  constraint "_copy_25" primary key ("id")
);
create unique index "param_key" on "sys_config" using btree (
  "param_key" asc
);
comment on column "sys_config"."param_key" is 'key';
comment on column "sys_config"."param_value" is 'value';
comment on column "sys_config"."status" is '状态   0：隐藏   1：显示';
comment on column "sys_config"."remark" is '备注';
comment on table "sys_config" is '系统配置信息表';

create table "sys_dict" (
  "id" varchar(32) not null,
  "group_id" varchar(32),
  "name" varchar(100),
  "value" varchar(64),
  "sort" int4,
  "status" int4,
  "remark" text,
  constraint "_copy_24" primary key ("id")
);
comment on column "sys_dict"."group_id" is '所属分组id';
comment on column "sys_dict"."name" is '字典名称';
comment on column "sys_dict"."value" is '字典值';
comment on column "sys_dict"."sort" is '排序号';
comment on column "sys_dict"."status" is '状态码';
comment on column "sys_dict"."remark" is '备注';
comment on table "sys_dict" is '数据字典';

create table "sys_dict_group" (
  "id" varchar(32) not null,
  "code" varchar(64) not null,
  "name" varchar(100),
  "create_time" timestamp,
  "remark" text,
  constraint "_copy_23" primary key ("id", "code")
);
comment on column "sys_dict_group"."code" is '分组编码';
comment on column "sys_dict_group"."name" is '分组名称';
comment on column "sys_dict_group"."create_time" is '创建时间';
comment on column "sys_dict_group"."remark" is '备注';
comment on table "sys_dict_group" is '数据字典分组';

create table "sys_log" (
  "id" varchar(32) not null,
  "user_name" varchar(50),
  "operation" varchar(50),
  "method" varchar(200),
  "params" varchar(5000),
  "time" int8 not null,
  "ip" varchar(64),
  "create_time" timestamp,
  constraint "_copy_22" primary key ("id")
);
comment on column "sys_log"."user_name" is '用户名';
comment on column "sys_log"."operation" is '用户操作';
comment on column "sys_log"."method" is '请求方法';
comment on column "sys_log"."params" is '请求参数';
comment on column "sys_log"."time" is '执行时长(毫秒)';
comment on column "sys_log"."ip" is 'ip地址';
comment on column "sys_log"."create_time" is '创建时间';
comment on table "sys_log" is '系统日志';

create table "sys_mail_log" (
  "id" varchar(32) not null,
  "sender" varchar(100) not null,
  "receiver" varchar(4000) not null,
  "subject" varchar(500) not null,
  "content" varchar(4000),
  "send_date" timestamp,
  "type" int2,
  "send_result" int2,
  "create_user_id" varchar(32),
  "create_user_org_no" varchar(32),
  constraint "_copy_21" primary key ("id")
);
comment on column "sys_mail_log"."sender" is '发送人';
comment on column "sys_mail_log"."receiver" is '接收人';
comment on column "sys_mail_log"."subject" is '邮件主题';
comment on column "sys_mail_log"."content" is '发送内容';
comment on column "sys_mail_log"."send_date" is '发送时间';
comment on column "sys_mail_log"."type" is '0：系统发送邮件 1：用户发送邮件';
comment on column "sys_mail_log"."send_result" is '发送结果 0:发送成功 1:发送失败';
comment on column "sys_mail_log"."create_user_id" is '创建者id';
comment on column "sys_mail_log"."create_user_org_no" is '创建人所属机构';
comment on table "sys_mail_log" is '邮件发送日志';

create table "sys_menu" (
  "menu_id" varchar(8) not null,
  "parent_id" varchar(8),
  "name" varchar(50),
  "url" varchar(200),
  "perms" varchar(500),
  "type" int2,
  "icon" varchar(50),
  "order_num" int4,
  constraint "_copy_20" primary key ("menu_id")
);
comment on column "sys_menu"."parent_id" is '父菜单id，一级菜单为0';
comment on column "sys_menu"."name" is '菜单名称';
comment on column "sys_menu"."url" is '菜单url';
comment on column "sys_menu"."perms" is '授权(多个用逗号分隔，如：user:list,user:create)';
comment on column "sys_menu"."type" is '类型   0：目录   1：菜单   2：按钮';
comment on column "sys_menu"."icon" is '菜单图标';
comment on column "sys_menu"."order_num" is '排序';
comment on table "sys_menu" is '菜单管理';

create table "sys_org" (
  "org_no" varchar(10) not null,
  "org_name" varchar(50),
  "parent_no" varchar(10),
  "org_type" int4,
  "status" int4,
  "sort" int4,
  "create_user_id" varchar(32),
  "create_user_org_no" varchar(32),
  "create_time" timestamp,
  constraint "_copy_19" primary key ("org_no")
);
comment on column "sys_org"."org_no" is '机构编码';
comment on column "sys_org"."org_name" is '部门名称';
comment on column "sys_org"."parent_no" is '上级部门id，一级部门为0';
comment on column "sys_org"."org_type" is '级别';
comment on column "sys_org"."status" is '状态  0：无效   1：有效';
comment on column "sys_org"."sort" is '排序';
comment on column "sys_org"."create_user_id" is '创建者id';
comment on column "sys_org"."create_user_org_no" is '创建人所属机构';
comment on column "sys_org"."create_time" is '创建时间';
comment on table "sys_org" is '组织机构';

create table "sys_oss" (
  "id" varchar(32) not null,
  "url" varchar(200),
  "create_user_id" varchar(32),
  "create_user_org_no" varchar(32),
  "create_date" timestamp,
  constraint "_copy_18" primary key ("id")
);
comment on column "sys_oss"."url" is 'url地址';
comment on column "sys_oss"."create_user_id" is '创建人所属机构';
comment on column "sys_oss"."create_user_org_no" is '创建人所属机构';
comment on column "sys_oss"."create_date" is '创建时间';
comment on table "sys_oss" is '文件上传';

create table "sys_printer" (
  "id" varchar(32) not null,
  "name" varchar(100) not null,
  "sn" varchar(100),
  "shops_id" varchar(100),
  "stub_sn" varchar(100),
  "user" varchar(100) not null,
  "ukey" varchar(100) not null,
  constraint "_copy_17" primary key ("id")
);
create index "sn" on "sys_printer" using btree (
  "sn" asc
);
create index "shops_id_copy_2" on "sys_printer" using btree (
  "shops_id" asc
);
comment on column "sys_printer"."name" is '打印机名称';
comment on column "sys_printer"."sn" is '打印机编号';
comment on column "sys_printer"."shops_id" is '所属门店';
comment on column "sys_printer"."stub_sn" is '存根打印机';
comment on column "sys_printer"."user" is '登录管理后台的账号名';
comment on column "sys_printer"."ukey" is '注册账号后生成的ukey';
comment on table "sys_printer" is '飞鹅打印机';

create table "sys_printer_copy1" (
  "id" varchar(32) not null,
  "name" varchar(100) not null,
  "sn" varchar(100),
  "shops_id" varchar(100),
  "stub_sn" varchar(100),
  "user" varchar(100) not null,
  "ukey" varchar(100) not null,
  constraint "_copy_16" primary key ("id")
);
create index "sn_copy_1" on "sys_printer_copy1" using btree (
  "sn" asc
);
create index "shops_id_copy_1" on "sys_printer_copy1" using btree (
  "shops_id" asc
);
comment on column "sys_printer_copy1"."name" is '打印机名称';
comment on column "sys_printer_copy1"."sn" is '打印机编号';
comment on column "sys_printer_copy1"."shops_id" is '所属门店';
comment on column "sys_printer_copy1"."stub_sn" is '存根打印机';
comment on column "sys_printer_copy1"."user" is '登录管理后台的账号名';
comment on column "sys_printer_copy1"."ukey" is '注册账号后生成的ukey';
comment on table "sys_printer_copy1" is '飞鹅打印机';

create table "sys_role" (
  "role_id" varchar(32) not null,
  "role_name" varchar(100),
  "remark" varchar(100),
  "create_user_id" varchar(32),
  "create_user_org_no" varchar(32),
  "create_time" timestamp,
  constraint "_copy_15" primary key ("role_id")
);
comment on column "sys_role"."role_name" is '角色名称';
comment on column "sys_role"."remark" is '备注';
comment on column "sys_role"."create_user_id" is '创建者id';
comment on column "sys_role"."create_user_org_no" is '创建者所属机构';
comment on column "sys_role"."create_time" is '创建时间';
comment on table "sys_role" is '角色';

create table "sys_role_menu" (
  "id" varchar(32) not null,
  "role_id" varchar(32),
  "menu_id" varchar(8),
  constraint "_copy_14" primary key ("id")
);
comment on column "sys_role_menu"."role_id" is '角色id';
comment on column "sys_role_menu"."menu_id" is '菜单id';
comment on table "sys_role_menu" is '角色与菜单对应关系';

create table "sys_role_org" (
  "id" varchar(32) not null,
  "role_id" varchar(32),
  "org_no" varchar(32),
  constraint "_copy_13" primary key ("id")
);
comment on column "sys_role_org"."role_id" is '角色id';
comment on column "sys_role_org"."org_no" is '部门id';
comment on table "sys_role_org" is '角色与机构对应关系';

create table "sys_sms_log" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "template_id" varchar(32),
  "code" varchar(32),
  "content" text,
  "mobile" text,
  "stime" timestamp,
  "sign" varchar(32),
  "send_status" int4,
  "send_id" varchar(32),
  "success_num" int4,
  "return_msg" varchar(50),
  constraint "_copy_12" primary key ("id")
);
comment on column "sys_sms_log"."id" is '主键';
comment on column "sys_sms_log"."user_id" is '操作人id';
comment on column "sys_sms_log"."template_id" is '模板id';
comment on column "sys_sms_log"."code" is '验证码';
comment on column "sys_sms_log"."content" is '发送内容（1-500 个汉字）utf-8编码';
comment on column "sys_sms_log"."mobile" is '手机号码。多个以英文逗号隔开';
comment on column "sys_sms_log"."stime" is '发送时间';
comment on column "sys_sms_log"."sign" is '必填参数。用户签名';
comment on column "sys_sms_log"."send_status" is '0成功 1失败';
comment on column "sys_sms_log"."send_id" is '发送编号';
comment on column "sys_sms_log"."success_num" is '成功提交数';
comment on column "sys_sms_log"."return_msg" is '返回消息';
comment on table "sys_sms_log" is '短信发送日志';

create table "sys_user" (
  "user_id" varchar(32) not null,
  "user_name" varchar(50) not null,
  "real_name" varchar(64) not null,
  "sex" int2 not null,
  "org_no" varchar(32),
  "salt" varchar(20),
  "email_host" varchar(32),
  "email" varchar(100),
  "email_pw" varchar(64),
  "mobile" varchar(100),
  "status" int2,
  "password" varchar(100),
  "create_user_id" varchar(32),
  "create_user_org_no" varchar(32),
  "create_time" timestamp,
  constraint "_copy_11" primary key ("user_id")
);
create unique index "username" on "sys_user" using btree (
  "user_name" asc
);
comment on column "sys_user"."user_name" is '用户名';
comment on column "sys_user"."org_no" is '机构编码';
comment on column "sys_user"."salt" is '盐';
comment on column "sys_user"."email_host" is '邮件服务器地址';
comment on column "sys_user"."email" is '邮箱';
comment on column "sys_user"."email_pw" is '用户邮箱密码';
comment on column "sys_user"."mobile" is '手机号';
comment on column "sys_user"."status" is '状态  0：禁用   1：正常';
comment on column "sys_user"."password" is '密码';
comment on column "sys_user"."create_user_id" is '创建者id';
comment on column "sys_user"."create_user_org_no" is '创建人所属机构';
comment on column "sys_user"."create_time" is '创建时间';
comment on table "sys_user" is '系统用户';

create table "sys_user_role" (
  "id" varchar(32) not null,
  "user_id" varchar(32),
  "role_id" varchar(32),
  constraint "_copy_10" primary key ("id")
);
comment on column "sys_user_role"."user_id" is '用户id';
comment on column "sys_user_role"."role_id" is '角色id';
comment on table "sys_user_role" is '用户与角色对应关系';

create table "sys_user_token" (
  "user_id" varchar(32) not null,
  "token" varchar(100) not null,
  "expire_time" timestamp,
  "update_time" timestamp,
  constraint "_copy_9" primary key ("user_id")
);
create unique index "token" on "sys_user_token" using btree (
  "token" asc
);
comment on column "sys_user_token"."token" is 'token';
comment on column "sys_user_token"."expire_time" is '过期时间';
comment on column "sys_user_token"."update_time" is '更新时间';
comment on table "sys_user_token" is '系统用户token';

create table "wx_account" (
  "appid" char(20) not null,
  "name" varchar(50) not null,
  "type" int2,
  "verified" int2,
  "secret" char(32) not null,
  "token" varchar(32),
  "aes_key" varchar(43),
  "content" text,
  constraint "_copy_8" primary key ("appid")
);
comment on column "wx_account"."appid" is 'appid';
comment on column "wx_account"."name" is '公众号名称';
comment on column "wx_account"."type" is '账号类型';
comment on column "wx_account"."verified" is '认证状态';
comment on column "wx_account"."secret" is 'appsecret';
comment on column "wx_account"."token" is 'token';
comment on column "wx_account"."aes_key" is 'aeskey';
comment on column "wx_account"."content" is '关注后自动回复';
comment on table "wx_account" is '公众号账号';

create table "wx_ma_config" (
  "id" varchar(32) not null,
  "app_id" varchar(64),
  "secret" varchar(128),
  "token" varchar(128),
  "aes_key" varchar(128),
  "msg_data_format" varchar(32),
  constraint "_copy_7" primary key ("id")
);
comment on column "wx_ma_config"."app_id" is '微信小程序appid';
comment on column "wx_ma_config"."secret" is '微信小程序secret';
comment on column "wx_ma_config"."token" is '小程序token';
comment on column "wx_ma_config"."aes_key" is '微信小程序encodingaeskey';
comment on column "wx_ma_config"."msg_data_format" is '消息格式，xml或者json';
comment on table "wx_ma_config" is '微信小程序配置';

create table "wx_msg" (
  "id" varchar(32) not null,
  "appid" char(20) not null,
  "openid" varchar(32) not null,
  "in_out" int2,
  "msg_type" char(25),
  "detail" text,
  "create_time" timestamp,
  constraint "_copy_6" primary key ("id")
);
create index "idx_appid" on "wx_msg" using btree (
  "appid" asc
);
comment on column "wx_msg"."id" is '主键';
comment on column "wx_msg"."appid" is 'appid';
comment on column "wx_msg"."openid" is '微信用户id';
comment on column "wx_msg"."in_out" is '消息方向';
comment on column "wx_msg"."msg_type" is '消息类型';
comment on column "wx_msg"."detail" is '消息详情';
comment on column "wx_msg"."create_time" is '创建时间';
comment on table "wx_msg" is '微信消息';
comment on index "idx_appid" is 'appid';

create table "wx_msg_reply_rule" (
  "rule_id" varchar(32) not null,
  "appid" char(20),
  "rule_name" varchar(20) not null,
  "match_value" varchar(200) not null,
  "exact_match" int2 not null,
  "reply_type" varchar(20) not null,
  "reply_content" varchar(1024) not null,
  "status" int2 not null,
  "desc" varchar(255),
  "effect_time_start" time,
  "effect_time_end" time,
  "priority" int8,
  "update_time" timestamp not null,
  constraint "_copy_5" primary key ("rule_id")
);
create index "idx_appid_rule" on "wx_msg_reply_rule" using btree (
  "appid" asc
);
comment on column "wx_msg_reply_rule"."appid" is 'appid';
comment on column "wx_msg_reply_rule"."rule_name" is '规则名称';
comment on column "wx_msg_reply_rule"."match_value" is '匹配的关键词、事件等';
comment on column "wx_msg_reply_rule"."exact_match" is '是否精确匹配';
comment on column "wx_msg_reply_rule"."reply_type" is '回复消息类型';
comment on column "wx_msg_reply_rule"."reply_content" is '回复消息内容';
comment on column "wx_msg_reply_rule"."status" is '规则是否有效';
comment on column "wx_msg_reply_rule"."desc" is '备注说明';
comment on column "wx_msg_reply_rule"."effect_time_start" is '生效起始时间';
comment on column "wx_msg_reply_rule"."effect_time_end" is '生效结束时间';
comment on column "wx_msg_reply_rule"."priority" is '规则优先级';
comment on column "wx_msg_reply_rule"."update_time" is '修改时间';
comment on table "wx_msg_reply_rule" is '自动回复规则';
comment on index "idx_appid" is 'appid';

create table "wx_msg_template" (
  "id" varchar(32) not null,
  "appid" char(20) not null,
  "template_id" varchar(100) not null,
  "name" varchar(50),
  "title" varchar(20),
  "content" text,
  "data" text,
  "url" varchar(255),
  "miniprogram" text,
  "status" int2 not null,
  "update_time" timestamp,
  constraint "_copy_4" primary key ("id")
);
create unique index "idx_name" on "wx_msg_template" using btree (
  "name" asc
);
create index "idx_status" on "wx_msg_template" using btree (
  "status" asc
);
create index "idx_appid_copy_1" on "wx_msg_template" using btree (
  "appid" asc
);
comment on column "wx_msg_template"."id" is 'id';
comment on column "wx_msg_template"."appid" is 'appid';
comment on column "wx_msg_template"."template_id" is '公众号模板id';
comment on column "wx_msg_template"."name" is '模版名称';
comment on column "wx_msg_template"."title" is '标题';
comment on column "wx_msg_template"."content" is '模板内容';
comment on column "wx_msg_template"."data" is '消息内容';
comment on column "wx_msg_template"."url" is '链接';
comment on column "wx_msg_template"."miniprogram" is '小程序信息';
comment on column "wx_msg_template"."status" is '是否有效';
comment on column "wx_msg_template"."update_time" is '修改时间';
comment on table "wx_msg_template" is '消息模板';
comment on index "idx_name" is '模板名称';
comment on index "idx_status" is '模板状态';
comment on index "idx_appid_copy_1" is 'appid';

create table "wx_pay_config" (
  "id" varchar(32) not null,
  "app_id" varchar(32) not null,
  "mch_id" varchar(32) not null,
  "mch_key" varchar(64) not null,
  "sub_app_id" varchar(32),
  "sub_mch_id" varchar(32),
  "key_path" varchar(255),
  constraint "_copy_3" primary key ("id")
);
create index "app_id" on "wx_pay_config" using btree (
  "app_id" asc
);
create index "mch_id" on "wx_pay_config" using btree (
  "mch_id" asc
);
comment on column "wx_pay_config"."app_id" is '微信小程序appid';
comment on column "wx_pay_config"."mch_id" is '微信支付商户号';
comment on column "wx_pay_config"."mch_key" is '微信支付商户密钥';
comment on column "wx_pay_config"."sub_app_id" is '服务商模式下的子商户公众账号id，普通模式请不要配置';
comment on column "wx_pay_config"."sub_mch_id" is '服务商模式下的子商户号，普通模式请不要配置';
comment on column "wx_pay_config"."key_path" is 'p12证书的位置';
comment on table "wx_pay_config" is '微信支付配置';

create table "wx_qr_code" (
  "id" varchar(32) not null,
  "appid" char(20) not null,
  "is_temp" int2,
  "scene_str" varchar(64),
  "ticket" varchar(255),
  "url" varchar(255),
  "expire_time" timestamp,
  "create_time" timestamp,
  constraint "_copy_2" primary key ("id")
);
create index "idx_appid_copy_2" on "wx_qr_code" using btree (
  "appid" asc
);
comment on column "wx_qr_code"."id" is 'id';
comment on column "wx_qr_code"."appid" is 'appid';
comment on column "wx_qr_code"."is_temp" is '是否为临时二维码';
comment on column "wx_qr_code"."scene_str" is '场景值id';
comment on column "wx_qr_code"."ticket" is '二维码ticket';
comment on column "wx_qr_code"."url" is '二维码图片解析后的地址';
comment on column "wx_qr_code"."expire_time" is '该二维码失效时间';
comment on column "wx_qr_code"."create_time" is '该二维码创建时间';
comment on table "wx_qr_code" is '公众号带参二维码';
comment on index "idx_appid_copy_2" is 'appid';

create table "wx_template_msg_log" (
  "log_id" varchar(32) not null,
  "appid" char(20) not null,
  "touser" varchar(50),
  "template_id" varchar(50),
  "data" text,
  "url" varchar(255),
  "miniprogram" text,
  "send_time" timestamp,
  "send_result" varchar(255),
  constraint "_copy_1" primary key ("log_id")
);
create index "idx_appid_msg_log" on "wx_template_msg_log" using btree (
  "appid" asc
);
comment on column "wx_template_msg_log"."log_id" is 'id';
comment on column "wx_template_msg_log"."appid" is 'appid';
comment on column "wx_template_msg_log"."touser" is '用户openid';
comment on column "wx_template_msg_log"."template_id" is 'templateid';
comment on column "wx_template_msg_log"."data" is '消息数据';
comment on column "wx_template_msg_log"."url" is '消息链接';
comment on column "wx_template_msg_log"."miniprogram" is '小程序信息';
comment on column "wx_template_msg_log"."send_time" is '发送时间';
comment on column "wx_template_msg_log"."send_result" is '发送结果';
comment on table "wx_template_msg_log" is '微信模版消息发送记录';
comment on index "idx_appid_copy_1" is 'appid';

alter table "act_ge_bytearray" add constraint "act_fk_bytearr_depl" foreign key ("deployment_id_") references "act_re_deployment" ("id_");
alter table "act_id_membership" add constraint "act_fk_memb_group" foreign key ("group_id_") references "act_id_group" ("id_");
alter table "act_id_membership" add constraint "act_fk_memb_user" foreign key ("user_id_") references "act_id_user" ("id_");
alter table "act_procdef_info" add constraint "act_fk_info_json_ba" foreign key ("info_json_id_") references "act_ge_bytearray" ("id_");
alter table "act_procdef_info" add constraint "act_fk_info_procdef" foreign key ("proc_def_id_") references "act_re_procdef" ("id_");
alter table "act_re_model" add constraint "act_fk_model_deployment" foreign key ("deployment_id_") references "act_re_deployment" ("id_");
alter table "act_re_model" add constraint "act_fk_model_source" foreign key ("editor_source_value_id_") references "act_ge_bytearray" ("id_");
alter table "act_re_model" add constraint "act_fk_model_source_extra" foreign key ("editor_source_extra_value_id_") references "act_ge_bytearray" ("id_");
alter table "act_ru_deadletter_job" add constraint "act_fk_deadletter_job_exception" foreign key ("exception_stack_id_") references "act_ge_bytearray" ("id_");
alter table "act_ru_deadletter_job" add constraint "act_fk_deadletter_job_execution" foreign key ("execution_id_") references "act_ru_execution" ("id_");
alter table "act_ru_deadletter_job" add constraint "act_fk_deadletter_job_process_instance" foreign key ("process_instance_id_") references "act_ru_execution" ("id_");
alter table "act_ru_deadletter_job" add constraint "act_fk_deadletter_job_proc_def" foreign key ("proc_def_id_") references "act_re_procdef" ("id_");
alter table "act_ru_event_subscr" add constraint "act_fk_event_exec" foreign key ("execution_id_") references "act_ru_execution" ("id_");
alter table "act_ru_execution" add constraint "act_fk_exe_parent" foreign key ("parent_id_") references "act_ru_execution" ("id_");
alter table "act_ru_execution" add constraint "act_fk_exe_procdef" foreign key ("proc_def_id_") references "act_re_procdef" ("id_");
alter table "act_ru_execution" add constraint "act_fk_exe_procinst" foreign key ("proc_inst_id_") references "act_ru_execution" ("id_");
alter table "act_ru_execution" add constraint "act_fk_exe_super" foreign key ("super_exec_") references "act_ru_execution" ("id_");
alter table "act_ru_identitylink" add constraint "act_fk_athrz_procedef" foreign key ("proc_def_id_") references "act_re_procdef" ("id_");
alter table "act_ru_identitylink" add constraint "act_fk_idl_procinst" foreign key ("proc_inst_id_") references "act_ru_execution" ("id_");
alter table "act_ru_identitylink" add constraint "act_fk_tskass_task" foreign key ("task_id_") references "act_ru_task" ("id_");
alter table "act_ru_job" add constraint "act_fk_job_exception" foreign key ("exception_stack_id_") references "act_ge_bytearray" ("id_");
alter table "act_ru_job" add constraint "act_fk_job_execution" foreign key ("execution_id_") references "act_ru_execution" ("id_");
alter table "act_ru_job" add constraint "act_fk_job_process_instance" foreign key ("process_instance_id_") references "act_ru_execution" ("id_");
alter table "act_ru_job" add constraint "act_fk_job_proc_def" foreign key ("proc_def_id_") references "act_re_procdef" ("id_");
alter table "act_ru_suspended_job" add constraint "act_fk_suspended_job_exception" foreign key ("exception_stack_id_") references "act_ge_bytearray" ("id_");
alter table "act_ru_suspended_job" add constraint "act_fk_suspended_job_execution" foreign key ("execution_id_") references "act_ru_execution" ("id_");
alter table "act_ru_suspended_job" add constraint "act_fk_suspended_job_process_instance" foreign key ("process_instance_id_") references "act_ru_execution" ("id_");
alter table "act_ru_suspended_job" add constraint "act_fk_suspended_job_proc_def" foreign key ("proc_def_id_") references "act_re_procdef" ("id_");
alter table "act_ru_task" add constraint "act_fk_task_exe" foreign key ("execution_id_") references "act_ru_execution" ("id_");
alter table "act_ru_task" add constraint "act_fk_task_procdef" foreign key ("proc_def_id_") references "act_re_procdef" ("id_");
alter table "act_ru_task" add constraint "act_fk_task_procinst" foreign key ("proc_inst_id_") references "act_ru_execution" ("id_");
alter table "act_ru_timer_job" add constraint "act_fk_timer_job_exception" foreign key ("exception_stack_id_") references "act_ge_bytearray" ("id_");
alter table "act_ru_timer_job" add constraint "act_fk_timer_job_execution" foreign key ("execution_id_") references "act_ru_execution" ("id_");
alter table "act_ru_timer_job" add constraint "act_fk_timer_job_process_instance" foreign key ("process_instance_id_") references "act_ru_execution" ("id_");
alter table "act_ru_timer_job" add constraint "act_fk_timer_job_proc_def" foreign key ("proc_def_id_") references "act_re_procdef" ("id_");
alter table "act_ru_variable" add constraint "act_fk_var_bytearray" foreign key ("bytearray_id_") references "act_ge_bytearray" ("id_");
alter table "act_ru_variable" add constraint "act_fk_var_exe" foreign key ("execution_id_") references "act_ru_execution" ("id_");
alter table "act_ru_variable" add constraint "act_fk_var_procinst" foreign key ("proc_inst_id_") references "act_ru_execution" ("id_");

INSERT INTO SYS_USER VALUES ('1', 'admin', '李鹏军', '1', '01', 'YzcmCZNvbXocrsz9dm8e', 'smtp.qq.com', '939961241@qq.com', '', '15209831990', '1', '9ec9750e709431dad22365cabc5c625482e574c74adaebba7dd02f1129e4ce1d', '', NULL, '2016-11-11 11:11:11');
INSERT INTO SYS_MENU VALUES ('10', '0', '系统设置', NULL, NULL, 0, 'system', 0);
INSERT INTO SYS_MENU VALUES ('1001', '10', '菜单管理', 'sys/menu', 'sys:menu:list,sys:menu:info', 1, 'menu', 1);
INSERT INTO SYS_MENU VALUES ('100101', '1001', '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100102', '1001', '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100103', '1001', '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('1002', '10', '组织机构', 'sys/org', 'sys:org:list,sys:org:info', 1, 'org', 2);
INSERT INTO SYS_MENU VALUES ('100201', '1002', '新增', NULL, 'sys:org:save', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100202', '1002', '修改', NULL, 'sys:org:update', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100203', '1002', '删除', NULL, 'sys:org:delete', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('1003', '10', '系统参数', 'sys/config', 'sys:config:list,sys:config:info', 1, 'xitongpeizhi', 3);
INSERT INTO SYS_MENU VALUES ('100301', '1003', '新增', NULL, 'sys:config:save', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100302', '1003', '修改', NULL, 'sys:config:update', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100303', '1003', '删除', NULL, 'sys:config:delete', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('1004', '10', '字典管理', 'sys/dictgroup', 'sys:dictgroup:list,sys:dictgroup:info,sys:dict:list,sys:dict:info', 1, 'dict', 4);
INSERT INTO SYS_MENU VALUES ('100401', '1004', '数据字典新增', NULL, 'sys:dict:save', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100402', '1004', '数据字典修改', NULL, 'sys:dict:update', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100403', '1004', '数据字典删除', NULL, 'sys:dict:delete', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100404', '1004', '数据字典分组新增', NULL, 'sys:dictgroup:save', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100405', '1004', '数据字典分组修改', NULL, 'sys:dictgroup:update', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100406', '1004', '数据字典分组删除', NULL, 'sys:dictgroup:delete', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('1005', '10', '文件上传', 'oss/oss', 'sys:oss:list', 1, 'oss', 5);
INSERT INTO SYS_MENU VALUES ('100501', '1005', '云存储配置', NULL, 'sys:oss:config', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100502', '1005', '删除', NULL, 'sys:oss:delete', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('1006', '10', '短信配置', 'sys/smslog', 'sys:smslog:list', 1, 'duanxin', 6);
INSERT INTO SYS_MENU VALUES ('100601', '1006', '修改配置', NULL, 'sys:smslog:config', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100602', '1006', '删除', NULL, 'sys:smslog:delete', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100603', '1006', '发送短信', NULL, 'sys:smslog:send', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('1007', '10', '定时任务', 'job/schedule', 'sys:schedule:list,sys:schedule:info', 1, 'job', 7);
INSERT INTO SYS_MENU VALUES ('100701', '1007', '删除', NULL, 'sys:schedule:delete', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100702', '1007', '暂停', NULL, 'sys:schedule:pause', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100703', '1007', '恢复', NULL, 'sys:schedule:resume', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100704', '1007', '立即执行', NULL, 'sys:schedule:run', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100705', '1007', '日志列表', NULL, 'sys:schedule:log', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100706', '1007', '新增', NULL, 'sys:schedule:save', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('100707', '1007', '修改', NULL, 'sys:schedule:update', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('1008', '10', '系统日志', 'sys/log', 'sys:log:list', 1, 'log', 8);
INSERT INTO SYS_MENU VALUES ('11', '0', '权限管理', NULL, NULL, 0, 'quanxian', 1);
INSERT INTO SYS_MENU VALUES ('1101', '11', '管理员列表', 'sys/user', 'sys:user:list,sys:user:info', 1, 'admin', 1);
INSERT INTO SYS_MENU VALUES ('110101', '1101', '重置密码', NULL, 'sys:user:resetPw', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('110102', '1101', '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('110103', '1101', '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('110104', '1101', '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('110105', '1101', '系统监控', null, 'sys:monitor:server', 2, null, 0);
INSERT INTO SYS_MENU VALUES ('1102', '11', '角色管理', 'sys/role', 'sys:role:list,sys:role:info', 1, 'role', 2);
INSERT INTO SYS_MENU VALUES ('110201', '1102', '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('110202', '1102', '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('110203', '1102', '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('12', '0', '开发工具', NULL, NULL, 0, 'dev', 2);
INSERT INTO SYS_MENU VALUES ('1201', '12', '令牌管理', 'sys/usertoken', 'sys:usertoken:list', 1, 'zaixian', 1);
INSERT INTO SYS_MENU VALUES ('120101', '1201', '删除', NULL, 'sys:usertoken:offline', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('1202', '12', '缓存信息', 'sys/redis', 'sys:cache:queryAll', 1, 'redis', 2);
INSERT INTO SYS_MENU VALUES ('120201', '1202', '删除', NULL, 'sys:cache:deleteCache', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('1203', '12', 'SQL监控', 'http://localhost:8888/platform-admin/druid/sql.html', NULL, 1, 'sql', 3);
INSERT INTO SYS_MENU VALUES ('1204', '12', 'admin接口文档', 'http://localhost:8888/platform-admin/doc.html', NULL, 1, 'interface', 4);
INSERT INTO SYS_MENU VALUES ('1205', '12', 'api接口文档', 'http://localhost:8889/platform-api/doc.html', NULL, 1, 'interface', 5);
INSERT INTO SYS_MENU VALUES ('1206', '12', '代码生成器', 'gen/generator', 'sys:generator:list', 1, 'code', 6);
INSERT INTO SYS_MENU VALUES ('120501', '1205', '生成代码', NULL, 'sys:generator:code', 2, NULL, 0);
INSERT INTO SYS_MENU VALUES ('13', '0', '公众号管理', '', '', 0, 'wechat', 3);
INSERT INTO SYS_MENU VALUES ('1301', '13', '公众号消息', 'wx/wx-msg', 'wx:wxmsg:list,wx:wxmsg:info,wx:wxuser:list', 1, 'duanxin', 1);
INSERT INTO SYS_MENU VALUES ('130101', '1301', '回复', '', 'wx:wxmsg:save', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('130102', '1301', '删除', '', 'wx:wxmsg:delete', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('1302', '13', '公众号菜单', 'wx/wx-menu', '', 1, 'leibie', 2);
INSERT INTO SYS_MENU VALUES ('130201', '1302', '更新公众号菜单', '', 'wx:menu:save', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('1303', '13', '消息模板', 'wx/msg-template', 'wx:msgtemplate:list,wx:msgtemplate:info', 1, 'tempmsg', 3);
INSERT INTO SYS_MENU VALUES ('130301', '1303', '新增', '', 'wx:msgtemplate:save', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('130302', '1303', '修改', '', 'wx:msgtemplate:update', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('130303', '1303', '删除', '', 'wx:msgtemplate:delete', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('1304', '13', '消息模版日志', 'wx/template-msg-log', 'wx:templatemsglog:list,wx:templatemsglog:info', 1, 'orders', 4);
INSERT INTO SYS_MENU VALUES ('130401', '1304', '删除', '', 'wx:templatemsglog:delete', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('1305', '13', '素材管理', 'wx/wx-assets', 'wx:wxassets:list', 1, 'files', 5);
INSERT INTO SYS_MENU VALUES ('130501', '1305', '新增修改', '', 'wx:wxassets:save', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('130502', '1305', '删除', '', 'wx:wxassets:delete', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('1306', '13', '草稿箱', 'wx/wx-draft', 'wx:draft:listDraft,wx:draft:getDraft', 1, '', 6);
INSERT INTO SYS_MENU VALUES ('130601', '1306', '新增', '', 'wx:draft:addDraft', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('130602', '1306', '修改', '', 'wx:draft:updateDraft', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('130603', '1306', '删除', '', 'wx:draft:delDraft', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('130604', '1306', '发布', '', 'wx:freepublish:submit', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('1307', '13', '发布记录', 'wx/wx-free-publish', 'wx:freepublish:getPublicationRecords,wx:freepublish:getArticleFromId', 1, '', 7);
INSERT INTO SYS_MENU VALUES ('130701', '1307', '删除', '', 'wx:freepublish:deletePush', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('1308', '13', '自动回复规则', 'wx/msg-reply-rule', 'wx:msgreplyrule:list,wx:msgreplyrule:info', 1, 'guize', 8);
INSERT INTO SYS_MENU VALUES ('130801', '1308', '新增', '', 'wx:msgreplyrule:save', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('130802', '1308', '修改', '', 'wx:msgreplyrule:update', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('130803', '1308', '删除', '', 'wx:msgreplyrule:delete', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('1309', '13', '带参二维码', 'wx/wx-qrcode', 'wx:wxqrcode:list,wx:wxqrcode:info', 1, 'qrcode', 9);
INSERT INTO SYS_MENU VALUES ('130901', '1309', '新增', '', 'wx:wxqrcode:save', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('130902', '1309', '删除', '', 'wx:wxqrcode:delete', 2, '', 0);
INSERT INTO SYS_MENU VALUES ('14', '0', '用户管理', '', '', 0, 'admin', 5);
INSERT INTO SYS_MENU VALUES ('1401', '14', '用户管理', 'wx/wx-user', 'wx:wxuser:list,wx:wxuser:info', 1, 'mp', 1);
INSERT INTO SYS_MENU VALUES ('140101', '1401', '标签操作', '', 'wx:wxuser:save', 2, '', 0);
